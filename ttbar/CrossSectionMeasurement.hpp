/*
 * \file CrossSectionMeasurement.h
 *
 */
#ifndef CrossSectionMeasurement_h
#define CrossSectionMeasurement_h

#include <utility>

#include "MyAnalysis.hpp"
#include "argparse.hpp"
#include <TLorentzVector.h>

class CrossSectionMeasurement: public MyAnalysis {
public:
  CrossSectionMeasurement(Config& _cfg);
  virtual ~CrossSectionMeasurement() { };

  virtual void CreateHistograms();
  virtual Bool_t Process(Long64_t entry);

  // number of total events
  int TotalEvents;
  double TotalEventsWeighted;
  // number of selected events
  int SelectedEvents;
  double SelectedEventsWeighted;

  //SignalToBackground
  Config cfg;
};

#endif
