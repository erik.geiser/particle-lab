/*
 * \file MassMeasurement.C
 *
 */

#include "MassMeasurement.hpp"
#include "argparse.hpp"
#include "tools.hpp"

#include <iostream>
#include <algorithm>
#include <utility>
#include <cmath>

#include <TH1D.h>
#include <TF1.h>
#include <TLorentzVector.h>

using namespace std;

const double Wmass = 80.385;

inline double W_mass_diff(pair<MyJet, MyJet>& p) {
  return abs((p.first + p.second).M() - Wmass);
};


inline double mass_diff(pair<MyJet, MyJet>& ttbjets, TLorentzVector& Wlep,
                        TLorentzVector& Whad) {
  return abs((Wlep + ttbjets.first).M() - (Whad + ttbjets.second).M());
}


MassMeasurement::MassMeasurement(Config& _cfg)
  : MyAnalysis()
{
  cfg = _cfg;
  MyJet::SetBTagScaleFactor(cfg.scale_factor);

  // initialize your variables here
  TotalEvents = 0;
  SelectedEvents = 0;
}

void MassMeasurement::CreateHistograms()
{
  // We tell ROOT that we use weighted histograms
  TH1::SetDefaultSumw2();


  hlep  = CreateHisto("Leptonic Branch", "m_{t,lep}", 17, 0, 340);
  hhad  = CreateHisto("Hadronic Branch", "m_{t,had}", 17, 0, 340);
  hcomb = CreateHisto("Combined Branches", "m_{t,combined}", 17, 0, 340);
  CreateHisto("m_{W,MET}", "W mass", 20, 0, 200);
  CreateHisto("m_{W,\\nu}", "W mass", 20, 0, 200);
  mlep = CreateHisto("Wlep","top mass from leptonic reconstruction",60,0,3000);
  mhad = CreateHisto("Whad","top mass from hadronic reconstruction",60,0,3000);
  CreateHisto("Mdiff","Mass differences",60,0,100);
}

Bool_t MassMeasurement::Process(Long64_t entry)
{
  GetEntry(entry);
  BuildEvent();

  TotalEvents++;

  if (TotalEvents % 10000 == 0 && !cfg.quiet)
    cout << "[*] MassMeasurement at Event: " << TotalEvents << endl;

  //////////////////////////////////////////////////////////////////////
  // implementation of systematic effects

  // As an example, we show how to apply the muon scale uncertainty
  for (unsigned int i = 0; i < Muons.size(); i++) {
    Muons[i] *= cfg.muon_scale;
  }

  //Jet scaling
  for (unsigned int i = 0; i < Jets.size(); i++) {
    Jets[i] *= cfg.jet_scale * (1.0 + cfg.jet_smear);
    if (Jets[i].IsBTagged() && cfg.manual_bscale) {
      Jets[i] *= cfg.BTagScale;
    }
  }

  //////////////////////////////////////////////////////////////////////
  // first, we identify all objects (muons, jets) by applying quality and
  // isolation requirements.

  vector<MyMuon> isomu;
  double hardestIsoMuonPt = 0;
  for (size_t i = 0; i < Muons.size(); i++) {
    if (Muons[i].IsIsolated()) {
      isomu.push_back(Muons[i]);
      if (Muons[i].Pt() > hardestIsoMuonPt) {
        hardestIsoMuonPt = Muons[i].Pt();
      }
    }
  }

  vector<MyJet> rjets;
  vector<MyJet> bjets;
  vector<MyJet> nonbjets;
  double hardestJetPt = 0;
  for (size_t i = 0; i < Jets.size(); i++) {
    if (Jets[i].GetJetID()) {
      rjets.push_back(Jets[i]);

      if (Jets[i].IsBTagged()) {
        bjets.push_back(Jets[i]);
      } else {
        nonbjets.push_back(Jets[i]);
      }

      if(rjets[i].Pt() > hardestJetPt){
        hardestJetPt = rjets[i].Pt();
      }
    }
  }

  ///////////////////////////////////////////////////////////////
  // Selection

  if (cfg.mass_cuts) {
    return kFALSE; // TODO: add cuts here
  }


  ///////////////////////////////////////////////////////////////
  // MassMeasurement

  if (bjets.size() < 2 || nonbjets.size() < 2 || isomu.size() != 1) return kFALSE;
  //if (!(bjets.size() >=2 && nonbjets.size()>=2 && isomu.size() == 1)) return kFALSE;
  SelectedEvents++;

  // Reconstruct hadronic W branch
  vector<pair<MyJet, MyJet>> Wjetpairs;
  for (size_t i = 0; i < nonbjets.size(); i++) {
    for (size_t j = 0; j < nonbjets.size(); j++) {
      if (i != j) {
        Wjetpairs.push_back(make_pair(nonbjets.at(i), nonbjets.at(j)));
      }
    }
  }

  pair<MyJet, MyJet> Wjets = Wjetpairs[0];

  if (Wjetpairs.size() > 1) {
    for (size_t i = 1; i < Wjetpairs.size(); i++) {
      if (W_mass_diff(Wjetpairs.at(i)) < W_mass_diff(Wjets)) {
        Wjets = Wjetpairs.at(i);
      }
    }
  }

  TLorentzVector Whad = Wjets.first + Wjets.second;


  for (auto& bjet: bjets){
    Fill("Whad",(Whad+bjet).M());
  }

  // Reconstruct leptonic W branch
  MyMuon mu = isomu[0];
  double theta = mu.Angle(met.Vect());
  double muonPz;
  double deltam2 = (pow(Wmass,2)-pow(mu.M(),2));
  if (!cfg.jopz) {
    muonPz = sqrt(
      pow((pow(Wmass, 2) - pow(mu.M(), 2)) / (mu.E() - 2 * mu.P() * cos(theta)), 2) -
      pow(-met.Et() - sqrt(pow(mu.Pt(), 2) + pow(mu.M(), 2)), 2)
    );
  } else{
    muonPz =
        (2*mu.Pz()*deltam2 + 2*mu.Pz()*(2*met.Px()*mu.Px()) + 2*mu.Pz()*(2*met.Py()*mu.Py()) )/(4*(pow(mu.E(),2)) - 4*(pow(mu.Pz(),2))) +
        sqrt(
            (2*mu.Pz()*deltam2 + 2*mu.Pz()*(2*met.Px()*mu.Px()) + 2*mu.Pz()*(2*met.Py()*mu.Py()) )/(4*(pow(mu.E(),2)) - 4*(pow(mu.Pz(),2))) +
            pow(deltam2,2) + pow((2*met.Px()*mu.Px()),2) + pow((2*met.Py()*mu.Py()),2) + 2*deltam2*(2*met.Px()*mu.Px())+2*deltam2*(2*met.Py()*mu.Py()) +
            2*(2*met.Py()*mu.Py())*(2*met.Px()*mu.Px()) - 4*(pow(met.Px(),2) + pow(met.Py(),2))
          );
  }
  TLorentzVector nu = TLorentzVector();
  double Enu = sqrt(pow(met.Px(), 2) + pow(met.Py(), 2) + pow(muonPz, 2));

  nu.SetPxPyPzE(met.Px(), met.Py(), muonPz, Enu);
  double positiveDiff = abs((nu + mu).M() - Wmass);

  nu.SetPxPyPzE(met.Px(), met.Py(), -muonPz, Enu);
  double negativeDiff = abs((nu + mu).M() - Wmass);

  if (negativeDiff > positiveDiff) nu.SetPxPyPzE(met.Px(), met.Py(), muonPz, Enu);

  Fill("m_{W,MET}", (isomu[0] + met).M());
  Fill("m_{W,\\nu}", (isomu[0] + nu).M());

  TLorentzVector Wlep = isomu[0] + (cfg.full_met? met : nu);

  for (auto& bjet: bjets){
    Fill("Wlep",(Wlep+bjet).M());
  }
  // Combine b jets with branches
  vector<pair<MyJet, MyJet>> bjetpairs; // first is leptonic, second is hadronic
  for (size_t i = 0; i < bjets.size(); i++) {
    for (size_t j = 0; j < bjets.size(); j++) {
      if (i != j) {
        //cout << "####### i: " << i << "####### j: " << j << endl;
        bjetpairs.push_back(make_pair(bjets.at(i), bjets.at(j)));
      }
    }
  }

  pair<MyJet, MyJet> ttbjets = bjetpairs[0];
  for (size_t i = 1; i < bjetpairs.size(); i++) {
    if (mass_diff(bjetpairs.at(i), Wlep, Whad) < mass_diff(ttbjets, Wlep, Whad)) {
      Fill("Mdiff",mass_diff(bjetpairs.at(i), Wlep, Whad));
      ttbjets = bjetpairs.at(i);
    }
  }

  double tM_lep = (Wlep + ttbjets.first).M();
  double tM_had = (Whad + ttbjets.second).M();


  Fill("Leptonic Branch", tM_lep);
  Fill("Hadronic Branch", tM_had);
  Fill("Combined Branches", tM_lep);
  Fill("Combined Branches", tM_had);

  return kTRUE;
}

void MassMeasurement::fitHists() {
  const char* formula = cfg.bw_mass_fit? "[0]/((x^2-[1]^2)^2+[1]^2*[2]^2)" : "gaus(0)";

  TF1* fitlep  = new TF1("fitlep", formula, 80, 240);
  TF1* fithad  = new TF1("fithad", formula, 80, 240);
  TF1* fitcomb = new TF1("fitcomb", formula, 80, 240);

  TF1* fitmlep = new TF1("fitmlep", formula, 70, 270);
  TF1* fitmhad = new TF1("fitmhad", formula, 70, 270);



  double m = 173;
  double gamma = 100;
  double c = cfg.bw_mass_fit? 10000000 : 20;
  fitlep->SetParameters(c / 2.0, m, gamma);
  fithad->SetParameters(c, m, gamma);
  fitcomb->SetParameters(c, m, gamma);

  fitmlep->SetParameters(c, m, gamma);
  fitmhad->SetParameters(c, m, gamma);

  fitlep->SetParLimits(1, 120, 260);
  fithad->SetParLimits(1, 120, 260);
  fitcomb->SetParLimits(1, 120, 260);


  fitlep->SetLineColor(1);
  fithad->SetLineColor(1);
  fitcomb->SetLineColor(1);

  fitmlep->SetLineColor(1);
  fitmhad->SetLineColor(1);


  hlep->Fit(fitlep, "R");
  hhad->Fit(fithad, "R");
  hcomb->Fit(fitcomb, "R");

  mlep->Fit(fitmlep,"R");
  mhad->Fit(fitmhad,"R");

  cout<< "#############lep int:"<<hlep->Integral() << endl;
  cout<< "#############had int:"<<hhad->Integral() << endl;
  cout<< "#############lep int:"<<mlep->Integral() << endl;
  cout<< "#############had int:"<<mhad->Integral() << endl;

  std::map<std::string, double> res;

  res["included_events"] = SelectedEvents;

  res["mass_lep"] = fitlep->GetParameter(1);
  res["mass_lep_err"] = fitlep->GetParError(1);
  res["gamma_lep"] = fitlep->GetParameter(0);
  res["gamma_lep_err"] = fitlep->GetParError(0);
  res["const_lep"] = fitlep->GetParameter(2);
  res["const_lep_err"] = fitlep->GetParError(2);

  res["mass_had"] = fithad->GetParameter(1);
  res["mass_had_err"] = fithad->GetParError(1);
  res["gamma_had"] = fithad->GetParameter(0);
  res["gamma_had_err"] = fithad->GetParError(0);
  res["const_had"] = fithad->GetParameter(2);
  res["const_had_err"] = fithad->GetParError(2);

  res["mass"] = fitcomb->GetParameter(1);
  res["mass_err"] = fitcomb->GetParError(1);
  res["gamma"] = fitcomb->GetParameter(0);
  res["gamma_err"] = fitcomb->GetParError(0);
  res["const"] = fitcomb->GetParameter(2);
  res["const_err"] = fitcomb->GetParError(2);

  res["#mass_lep_no_comb#"] = fitmlep->GetParameter(1);
  res["#mass_lep_no_comb_err#"] = fitmlep->GetParError(1);
  res["#mass_had_no_comb#"] = fitmhad->GetParameter(1);
  res["#mass_had_no_comb_err#"] = fitmhad->GetParError(1);


  save_json("results/" + cfg.output_dir+ "/" + cfg.json_name, res);
}
