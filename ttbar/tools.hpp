#ifndef tools_hpp
#define tools_hpp

#include <sstream>
#include <fstream>
#include <iostream>
#include <map>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <TStyle.h>
#include <TROOT.h>


template <typename T>
void save_json(std::string filename, const std::map<std::string, T>& map) {
  boost::property_tree::ptree pt;
  for (auto& entry: map)
      pt.put<T>(entry.first, entry.second);
  std::ostringstream json_buffer;
  boost::property_tree::write_json(json_buffer, pt, true);
  std::cout << json_buffer.str() << std::endl;

  std::fstream outfile;
  outfile.open(filename, std::fstream::out);
  outfile << json_buffer.str();
  outfile.close();
}

inline void setRootStyle() {
  gROOT->Reset();
  //gROOT->SetStyle("Plain");

  TStyle *MyStyle = new TStyle("MyStyle","My Root Styles");
  MyStyle->SetStatColor(0);
  MyStyle->SetCanvasColor(0);
  MyStyle->SetPadColor(0);
  MyStyle->SetPadBorderMode(0);
  MyStyle->SetCanvasBorderMode(0);
  MyStyle->SetFrameBorderMode(0);
  MyStyle->SetOptStat(0);
  MyStyle->SetStatBorderSize(2);
  MyStyle->SetOptTitle(0);
  MyStyle->SetPadTickX(1);
  MyStyle->SetPadTickY(1);
  MyStyle->SetPadBorderSize(2);
  MyStyle->SetPalette(51, 0);
  MyStyle->SetPadBottomMargin(0.15);
  MyStyle->SetPadTopMargin(0.05);
  MyStyle->SetPadLeftMargin(0.15);
  MyStyle->SetPadRightMargin(0.25);
  MyStyle->SetTitleColor(1);
  MyStyle->SetTitleFillColor(0);
  MyStyle->SetTitleFontSize(0.03);
  MyStyle->SetTitleX(0.15);
  MyStyle->SetTitleBorderSize(0);
  MyStyle->SetLineWidth(1);
  MyStyle->SetHistLineWidth(3);
  MyStyle->SetLegendBorderSize(0);
  MyStyle->SetNdivisions(502, "x");
  MyStyle->SetMarkerSize(0.8);
  MyStyle->SetTickLength(0.03);
  MyStyle->SetTitleOffset(1.5, "x");
  MyStyle->SetTitleOffset(1.5, "y");
  MyStyle->SetTitleOffset(1.0, "z");
  MyStyle->SetLabelSize(0.05, "x");
  MyStyle->SetLabelSize(0.05, "y");
  MyStyle->SetLabelSize(0.05, "z");
  MyStyle->SetLabelOffset(0.03, "x");
  MyStyle->SetLabelOffset(0.03, "y");
  MyStyle->SetLabelOffset(0.03, "z");
  MyStyle->SetTitleSize(0.05, "x");
  MyStyle->SetTitleSize(0.05, "y");
  MyStyle->SetTitleSize(0.05, "z");
  gROOT->SetStyle("MyStyle");
}


#endif
