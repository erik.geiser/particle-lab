/*
 * \file MassMeasurement.h
 *
 */
#ifndef MassMeasurement_h
#define MassMeasurement_h

#include <utility>

#include "MyAnalysis.hpp"
#include "argparse.hpp"
#include <TLorentzVector.h>

class MassMeasurement: public MyAnalysis {
public:
  MassMeasurement(Config& _cfg);
  virtual ~MassMeasurement() { };

  virtual void CreateHistograms();
  virtual Bool_t Process(Long64_t entry);
  virtual void fitHists();
  friend inline double W_mass_diff(std::pair<MyJet, MyJet>& p);
  friend inline double mass_diff(std::pair<MyJet, MyJet>& ttbjets, TLorentzVector& Wlep,
                                 TLorentzVector& Whad);

  // number of total events
  int TotalEvents;
  // number of selected events
  int SelectedEvents;

  //SignalToBackground
  Config cfg;

  TH1D* hlep;
  TH1D* hhad;
  TH1D* hcomb;
  TH1D* mlep;
  TH1D* mhad;

};

#endif
