/*
 * \file TTBarAnalysis.h
 * \author Martin Weber
 *
 */
#ifndef TTBarAnalysis_h
#define TTBarAnalysis_h

#include "MyAnalysis.hpp"
#include "argparse.hpp"

class TTBarAnalysis: public MyAnalysis {
public:
  TTBarAnalysis(Config& _cfg);
  virtual ~TTBarAnalysis() { };

  virtual void CreateHistograms();
  virtual Bool_t Process(Long64_t entry);

  // number of total events
  int TotalEvents;
  // number of selected events
  int SelectedEvents;
  Config cfg;
};

#endif
