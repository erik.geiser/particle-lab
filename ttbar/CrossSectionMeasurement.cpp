/*
 * \file CrossSectionMeasurement.C
 *
 */

#include "CrossSectionMeasurement.hpp"
#include "argparse.hpp"

#include <iostream>
#include <algorithm>
#include <utility>
#include <cmath>

#include <TH1D.h>
#include <TLorentzVector.h>

using namespace std;


CrossSectionMeasurement::CrossSectionMeasurement(Config& _cfg)
  : MyAnalysis()
{
  cfg = _cfg;
  MyJet::SetBTagScaleFactor(cfg.scale_factor);

  // initialize your variables here
  TotalEvents = 0;
  TotalEventsWeighted = 0;
  SelectedEvents = 0;
  SelectedEventsWeighted = 0;
}

void CrossSectionMeasurement::CreateHistograms()
{
  // We tell ROOT that we use weighted histograms
  TH1::SetDefaultSumw2();

  /*
  CreateHisto("tMlep", "Top Quark Mass (leptonic branch)", 60, 0, 300);
  CreateHisto("tMhad", "Top Quark Mass (hadronic branch)", 60, 0, 300);
  CreateHisto("tMcombined", "Top Quark Mass (Combined branches)", 60, 0, 300);
  */
  CreateHisto("isopt", "Muon iso pt", 10, 0, 200);
}

Bool_t CrossSectionMeasurement::Process(Long64_t entry)
{
  GetEntry(entry);
  BuildEvent();

  TotalEvents++;
  TotalEventsWeighted += EventWeight;

  if (TotalEvents % 10000 == 0 && !cfg.quiet)
    cout << "[*] CrossSectionMeasurement at Event: " << TotalEvents << endl;

  //////////////////////////////////////////////////////////////////////
  // implementation of systematic effects

  // As an example, we show how to apply the muon scale uncertainty
  for (unsigned int i = 0; i < Muons.size(); i++) {
    Muons[i] *= cfg.muon_scale;
  }

  //Jet scaling
  for (unsigned int i = 0; i < Jets.size(); i++) {
    Jets[i] *= cfg.jet_scale * (1.0 + cfg.jet_smear);
    if (Jets[i].IsBTagged() && cfg.manual_bscale) {
      Jets[i] *= cfg.BTagScale;
    }
  }

  //////////////////////////////////////////////////////////////////////
  // first, we identify all objects (muons, jets) by applying quality and
  // isolation requirements.

  vector<MyMuon> isomu;
  double hardestIsoMuonPt = 0;
  for (size_t i = 0; i < Muons.size(); i++) {
    if (Muons[i].IsIsolated()) {
      isomu.push_back(Muons[i]);
      if (Muons[i].Pt() > hardestIsoMuonPt) {
        hardestIsoMuonPt = Muons[i].Pt();
      }
    }
  }

  vector<MyJet> rjets;
  vector<MyJet> bjets;
  vector<MyJet> nonbjets;
  double hardestJetPt = 0;
  for (size_t i = 0; i < Jets.size(); i++) {
    if (Jets[i].GetJetID()) {
      rjets.push_back(Jets[i]);

      if (Jets[i].IsBTagged()) {
        bjets.push_back(Jets[i]);
      } else {
        nonbjets.push_back(Jets[i]);
      }

      if(rjets[i].Pt() > hardestJetPt){
        hardestJetPt = rjets[i].Pt();
      }
    }
  }

  //////////////////////////////////////////////////////////////////////
  // trigger
  bool triggered = (isomu.size() > 0) && (hardestIsoMuonPt > 26); // above turn on

  //////////////////////////////////////////////////////////////////////
  // Njets
  bool enough_rjets = (rjets.size() > 2);


  //////////////////////////////////////////////////////////////////////
  // Bjets
  bool bjets_present = (bjets.size() > 0);

  //////////////////////////////////////////////////////////////////////
  // JetPt
  bool jet_pt_above = false;
  for (auto& jet : rjets) {
    if (abs(jet.Pt()) > 45) {
      jet_pt_above = true;
      break;
    }
  }

  //////////////////////////////////////////////////////////////////////
  // MET
  bool met_above_threshold = (met.E() > 50);

  // eta muon
  //////////////////////////////////////////////////////////////////////
  // MuonPt
  bool muon_pt_below_threshold = false;
  for (auto& mu : isomu) {
    if (!(mu.Pt() > 125)) {
      muon_pt_below_threshold = true;
      break;
    }
  }

  //////////////////////////////////////////////////////////////////////
  // Drell Yan reduction
  bool muon_from_z = !(isomu.size() == 2 && (isomu[0] + isomu[1]).M() >= 86 &&
                     (isomu[0] + isomu[1]).M() <= 140);

  //////////////////////////////////////////////////////////////////////
  // Reconstructed W leptons
  bool w_from_leptons = false;
  if (isomu.size() == 1 ) {
    if ((isomu[0]+met).M() > 82 || (isomu[0]+met).M() < 78) {
      w_from_leptons = true;
    }
  }

  //////////////////////////////////////////////////////////////////////
  // Wmass
  bool Wreconstruct_at_least = false;
  vector<MyJet> Wreconstruction;
  double Wjet=0;
  for (auto& jet : rjets) {
    if(!jet.IsBTagged()){
      Wreconstruction.push_back(jet);
    }
  }
  if (Wreconstruction.size() == 2) {
    for (unsigned int i = 0; i < Wreconstruction.size(); i++) {
      Wjet += Wreconstruction[i].E();
    }
    if (Wjet > 80) {
      Wreconstruct_at_least = true;
    }
  }

  //////////////////////////////////////////////////////////////////////
  // NIsoMu
  bool enough_iso_muons = (isomu.size() == 1);


  if (triggered){
    if (enough_rjets){
      if (bjets_present){
        if (jet_pt_above){
          if (muon_pt_below_threshold){
            if (muon_from_z){
              if (enough_iso_muons){
                SelectedEvents++;
                SelectedEventsWeighted += EventWeight;
                if (!met_above_threshold){
                  if (!w_from_leptons){
                    if (Wreconstruct_at_least){
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }


  return kTRUE;
}
