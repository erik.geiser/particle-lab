#ifndef argparse_hpp
#define argparse_hpp

#include <iostream>
#include <string>
#include <boost/program_options.hpp>


namespace po = boost::program_options;


struct Config {
  double lumi{50};
  double BTagScale{0.95};
  double weight_factor{1.0};
  double jet_scale{1.0};
  double jet_smear{0.0};
  double muon_scale{1.0};
  double scale_factor{1.0};
  double mc_efficiency{0};
  int data_bg{0};
  bool full_met{false};
  bool mass_cuts{false};
  bool blind{false};
  bool quiet{false};
  bool noplots{false};
  bool bw_mass_fit{false};
  bool manual_bscale{false};
  bool exclude_prior_selection{false};
  bool jopz{false};
  std::string mode{""};
  std::string data_dir{"files/"};
  std::string output_dir{""};
  std::string json_name{"eventnumbers.json"};
};


inline Config get_config(int argc, char* argv[]) {
  Config cfg;

  try {
    po::options_description params("Parameters");

    params.add_options()
        ("lumi", po::value<double>(&cfg.lumi), "Luminosity")
        ("BTagScale", po::value<double>(&cfg.BTagScale), "b-tagging scale factor")
        ("weight_factor", po::value<double>(&cfg.weight_factor), "Additional event weight")
        ("jet_scale", po::value<double>(&cfg.jet_scale), "Jet scale factor")
        ("jet_smear", po::value<double>(&cfg.jet_smear), "Jet smearing")
        ("muon_scale", po::value<double>(&cfg.muon_scale), "Muon Scale")
        ("scale_factor", po::value<double>(&cfg.scale_factor), "Geometric scale factor")
        ("mc_efficiency", po::value<double>(&cfg.mc_efficiency), "Efficiency on MC sample")
        ("data_bg", po::value<int>(&cfg.data_bg), "Expected background on data")
        ("full_met", po::bool_switch(&cfg.full_met), "Reconstruct W with full MET")
        ("mass_cuts", po::bool_switch(&cfg.mass_cuts), "Apple xsec cuts on mass measurement")
        ("blind", po::bool_switch(&cfg.blind), "Exclude data in analysis")
        ("quiet", po::bool_switch(&cfg.quiet), "Don't print out event progress")
        ("bw_mass_fit", po::bool_switch(&cfg.bw_mass_fit), "Use Breit Wigner for mass fit")
        ("manual_bscale", po::bool_switch(&cfg.manual_bscale), "Rescale b jets manually")
        ("noplots", po::bool_switch(&cfg.noplots), "Do not make any plots")
        ("exclude_prior_selection", po::bool_switch(&cfg.exclude_prior_selection),
         "Exclude all and trigger bins in selection")
         ("jopz", po::bool_switch(&cfg.jopz), "johannes pz formula")
        ("mode", po::value<std::string>(&cfg.mode), "The step which will be executed")
        ("data_dir", po::value<std::string>(&cfg.data_dir), "Directory with root trees")
        ("output_dir", po::value<std::string>(&cfg.output_dir), "Output directory")
        ("json_name", po::value<std::string>(&cfg.json_name), "Result json file name")
    ;
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, params), vm);
    po::notify(vm);

    if (cfg.data_dir.back() != '/') cfg.data_dir += "/";
  } catch(std::exception& e) {
    std::cerr << e.what() << "\n";
  } catch(...) {
    std::cerr << "Exception of unknown type!\n";
  }

  return cfg;
}

#endif
