/*
 * \file EventSelection.h
 *
 */
#ifndef EventSelection_h
#define EventSelection_h

#include "MyAnalysis.hpp"
#include "argparse.hpp"

class EventSelection: public MyAnalysis {
public:
  EventSelection(Config& _cfg);
  virtual ~EventSelection() { };

  virtual void CreateHistograms();
  virtual Bool_t Process(Long64_t entry);

  // number of total events
  int TotalEvents;
  double TotalEventsWeighted;

  // number of selected events
  int SelectedEvents;
  double SelectedEventsWeighted;

  //SignalToBackground
  double SignalToBackground;
  Config cfg;
  TH1D* cutflow;
};

#endif
