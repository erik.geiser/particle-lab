/*
 * \file Investigation.h
 *
 */
#ifndef Investigation_hpp
#define Investigation_hpp

#include "MyAnalysis.hpp"
#include "argparse.hpp"

class Investigation: public MyAnalysis {
public:
  Investigation(Config& _cfg);
  virtual ~Investigation() { };

  virtual void CreateHistograms();
  virtual Bool_t Process(Long64_t entry);
  virtual void HistFit();

  //virtual Double_t mybw(Double_t* x, Double_t* par);
  // number of total events
  int TotalEvents;
  // number of selected events
  int SelectedEvents;
  Config cfg;

  TH1D * h;
};

#endif
