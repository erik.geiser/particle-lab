/*
 * \file TTBarAnalysis.C
 * \author Martin Weber
 *
 */

#include "TTBarAnalysis.hpp"
#include "argparse.hpp"

#include <iostream>
#include <algorithm>

#include <TH1D.h>

using namespace std;

TTBarAnalysis::TTBarAnalysis(Config& _cfg)
  : MyAnalysis()
{
  cfg = _cfg;
  MyJet::SetBTagScaleFactor(cfg.scale_factor);

  // initialize your variables here
  TotalEvents = 0;
  SelectedEvents = 0;
}

void TTBarAnalysis::CreateHistograms()
{
  // We tell ROOT that we use weighted histograms
  TH1::SetDefaultSumw2();

  // First create a "cut flow" histogram that contains event counts for the
  // different stages of the analysis
  CreateHisto("cutflow", "cut flow", 10, 0, 10);

  // For a cut flow histogram, it is important that we initialize all bins we
  // are using with zeroes before processing the first event. If this is
  // forgotten, the cut flow histogram may contain garbage, so be careful!
  // It will also be useful to put proper names, e.g. "pT" if you cut on pT.
  // You need to use the same names below in your analysis code!
  Fill("cutflow", "all", 0);
  Fill("cutflow", "trigger", 0);
  Fill("cutflow", "BJets", 0);
  Fill("cutflow", "3rd", 0);
  Fill("cutflow", "4th", 0);
  Fill("cutflow", "5th", 0);
  Fill("cutflow", "6th", 0);
  Fill("cutflow", "7th", 0);
  Fill("cutflow", "8th", 0);
  Fill("cutflow", "9th", 0);

  // Now we create all the other histograms that we use in our selection.

  // The first string is the name of the histogram, the second string is the
  // title of the histogram that is used for labelling the x-axis. The three
  // numbers specify the number of bins, and the lower and upper bound of the
  // histogram.
  CreateHisto("Muon_Pt", "Pt of all muons [GeV]", 50, 0, 250);
  CreateHisto("NIsoMuon", "Number of isolated muons", 10, 0, 10);
}

Bool_t TTBarAnalysis::Process(Long64_t entry)
{
  // The Process() function is called for each event in the ROOT TTree. The
  // entry argument specifies which entry in the currently loaded tree is to
  // be processed.
  //
  // The return value is currently not used.

  // We load the event from the ROOT tree. After this we can access the
  // variables of the event.
  GetEntry(entry);

  // This fills the Muons, Electrons, Photons, Jets and MC information from
  // the ROOT TTree. After this we can use e.g. Muons[0]
  BuildEvent();

  // We count the number of events that we process (e.g. for efficiency calculation).
  ++TotalEvents;
  // Inform us every 10000 events
  if (TotalEvents % 10000 == 0 && !cfg.quiet)
    cout << "[*] TTBarAnalysis in Event: " << TotalEvents << endl;

  // fill the cutflow histogram at the beginning and after each cut
  Fill("cutflow", "all");

  //////////////////////////////////////////////////////////////////////
  // implementation of systematic effects

  // As an example, we show how to apply the muon scale uncertainty
  for (unsigned int i = 0; i < Muons.size(); i++) {
    Muons[i] *= cfg.muon_scale;
  }

	//Jet scaling
	for (unsigned int i = 0; i < Jets.size(); i++) {
    Jets[i] *= cfg.jet_scale;
  }


  //////////////////////////////////////////////////////////////////////
  // first, we identify all objects (muons, jets) by applying quality and
  // isolation requirements.

  // Muon isolation
  vector<int> isomu;
  for (unsigned int i = 0; i < Muons.size(); i++) {
    if (Muons[i].IsIsolated()) {
      isomu.push_back(i);
      Fill("Muon_Pt", Muons[i].Pt());
      // you can also access Muons[i].E(), .Px(), .Py(), .Pz(), .Eta(), .Phi(), ...
      // and the same with Electrons, Photons, Jets, ...
      // see Documentation of ROOT TLorentzVector
    }
  }
  int NIsoMuon = isomu.size();
  Fill("NIsoMuon", NIsoMuon);

  // now start here applying the jet ID...
	vector<MyJet> BJets;
	vector<MyJet> reducedJets;
	for (unsigned int i = 0; i < Jets.size(); i++) {
		if (Jets[i].GetJetID() ){
			if (Jets[i].IsBTagged() ){
				BJets.push_back(Jets[i]);
			}
			reducedJets.push_back(Jets[i]);
		}

	}
  //////////////////////////////////////////////////////////////////////
  // after all objects have been identified, the selection can start

  // the first requirement should be the trigger requirement...

	if (NIsoMuon == 1){
  	Fill("cutflow", "trigger");
	}

  // ... and then you have to add more code for the selection here ...
	if (BJets.size() >= 2) {
			Fill("cutflow","BJets");
	}


  // ... end of the selection. Count selected events
  SelectedEvents++;

  // When the event has been accepted, we return kTRUE, if it is rejected, we
  // return kFALSE.
  return kTRUE;
}
