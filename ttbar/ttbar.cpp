#include "TTBarAnalysis.hpp"
#include "Investigation.hpp"
#include "TriggerEfficiency.hpp"
#include "EventSelection.hpp"
#include "CrossSectionMeasurement.hpp"
#include "MassMeasurement.hpp"
#include "Plotter.hpp"
#include "argparse.hpp"
#include "tools.hpp"
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <TChain.h>
#include <TGraphAsymmErrors.h>
#include <TF1.h>
#include <TFile.h>
#include <TStyle.h>


std::vector<string> dataset_files{"data.root", "ttbar.root", "wjets.root", "dy.root", "ww.root",
                                  "wz.root", "zz.root", "qcd.root"};
std::vector<string> dataset_names{"Data", "TTBar", "Wjets", "DY", "WW", "WZ", "ZZ", "QCD"};


void run_investigation(Config& cfg) {
  Plotter P;
  std::map<string, Investigation*> analyses;
  std::map<string, TChain*> chains;

  size_t i = cfg.blind? 1 : 0;
  for (; i < dataset_files.size(); i++) {
    analyses[dataset_names[i]] = new Investigation(cfg);
    chains[dataset_names[i]] = new TChain("events");
    chains[dataset_names[i]]->Add((cfg.data_dir + dataset_files[i]).c_str());
    chains[dataset_names[i]]->Process(analyses[dataset_names[i]]);

    if (i == 0) { // data
      P.SetData(analyses[dataset_names[i]]->histo, dataset_names[i]);
    } else {
      P.AddBg(analyses[dataset_names[i]]->histo, dataset_names[i]);
    }
    if ((dataset_names[i] == "DY") || (dataset_names[i] == "Data")) {
      analyses[dataset_names[i]]->HistFit();
    }
  }
  if (!cfg.noplots) {
    P.Plot(string("results/investigation/plots_log.pdf"), true);
    P.Plot(string("results/investigation/plots.pdf"), false);
  }
}


void run_trigger_efficiency(Config& cfg) {
  TriggerEfficiency* ttbar = new TriggerEfficiency(cfg);
  TChain* chain = new TChain("events");
  chain->Add((cfg.data_dir + "ttbar.root").c_str());
  chain->Process(ttbar);
  if (!cfg.noplots) ttbar->finalize();
}


void run_event_selection(Config& cfg) {
  if (cfg.output_dir == "") cfg.output_dir = "event_selection";

  Plotter P;
  std::map<string, EventSelection*> analyses;
  std::map<string, TChain*> chains;


  // i = 1 explicitly excludes the data from the plots
  for (size_t i = 0; i < dataset_files.size(); i++) {
    analyses[dataset_names[i]] = new EventSelection(cfg);
    chains[dataset_names[i]] = new TChain("events");
    chains[dataset_names[i]]->Add((cfg.data_dir + dataset_files[i]).c_str());
    chains[dataset_names[i]]->Process(analyses[dataset_names[i]]);

    if (i == 0) { // data
      P.SetData(analyses[dataset_names[i]]->histo, dataset_names[i]);
    } else {
    P.AddBg(analyses[dataset_names[i]]->histo, dataset_names[i]);
    }
  }

  if (!cfg.noplots) {
    TH1D* sgn = (TH1D*) analyses["TTBar"]->cutflow->Clone("stobg");
    TH1D* bg = (TH1D*) analyses[dataset_names[2]]->cutflow->Clone("bg");
    for (size_t i = 3; i < dataset_files.size(); i++) {
      bg->Add(analyses[dataset_names[i]]->cutflow);
    }
    sgn->Divide(bg);

    gStyle->SetOptStat(0);
    sgn->SetTitle("Signal To Noise Ratio");
    sgn->SetXTitle("Cuts");
    TCanvas c;
    sgn->Draw("LF2");
    c.Print("results/event_selection/signaltonoise.pdf");

    P.Plot(string("results/event_selection/selection_log.pdf"), true);
    P.Plot(string("results/event_selection/selection_lin.pdf"), false);
  }

  std::map<std::string, int> res;
  res["lumi"] = cfg.lumi;
  res["sgn_selected"] = analyses["TTBar"]->SelectedEvents;
  res["sgn_total"] = analyses["TTBar"]->TotalEvents;
  res["sgn_selected_weighted"] = analyses["TTBar"]->SelectedEventsWeighted;
  res["sgn_total_weighted"] = analyses["TTBar"]->TotalEventsWeighted;
  res["bg_selected"] = 0;
  res["bg_total"] = 0;
  res["bg_selected_weighted"] = 0;
  res["bg_total_weighted"] = 0;
  for (size_t i = 2; i < dataset_names.size(); i++) {
    res["bg_selected"] += analyses[dataset_names[i]]->SelectedEvents;
    res["bg_total"] += analyses[dataset_names[i]]->TotalEvents;
    res["bg_selected_weighted"] += analyses[dataset_names[i]]->SelectedEventsWeighted;
    res["bg_total_weighted"] += analyses[dataset_names[i]]->TotalEventsWeighted;
  }
  save_json("results/" + cfg.output_dir + "/" + cfg.json_name, res);
}


void run_cross_section_measurement(Config& cfg) {
  CrossSectionMeasurement* meas = new CrossSectionMeasurement(cfg);
  TChain* chain = new TChain("events");
  chain->Add((cfg.data_dir + "data.root").c_str());
  chain->Process(meas);

  std::map<std::string, double> res;
  res["xsec"] = (meas->SelectedEventsWeighted - cfg.data_bg) / (cfg.lumi * cfg.mc_efficiency);
  res["xsec_theo_upper"] = (meas->SelectedEventsWeighted - 1.1 * cfg.data_bg) /
                           (cfg.lumi * cfg.mc_efficiency);
  res["xsec_theo_lower"] = (meas->SelectedEventsWeighted - 0.9 * cfg.data_bg) /
                           (cfg.lumi * cfg.mc_efficiency);
  res["total"] = meas->TotalEvents;
  res["total_weighted"] = meas->TotalEventsWeighted;
  res["selected"] = meas->SelectedEvents;
  res["selected_weighted"] = meas->SelectedEventsWeighted;
  res["expected_bg"] = cfg.data_bg;
  save_json("results/measurement/" + cfg.json_name, res);
}


void run_mass_measurement(Config& cfg) {
  Plotter P;
  std::map<string, MassMeasurement*> mcs;
  std::map<string, TChain*> mc_chains;

  MassMeasurement* meas = new MassMeasurement(cfg);
  TChain* chain = new TChain("events");
  chain->Add((cfg.data_dir + "data.root").c_str());
  chain->Process(meas);

  for (size_t i = 1; i < dataset_files.size(); i++) {
    mcs[dataset_names[i]] = new MassMeasurement(cfg);
    mc_chains[dataset_names[i]] = new TChain("events");
    mc_chains[dataset_names[i]]->Add((cfg.data_dir + dataset_files[i]).c_str());
    mc_chains[dataset_names[i]]->Process(mcs[dataset_names[i]]);
    P.AddBg(mcs[dataset_names[i]]->histo, dataset_names[i]);
    if (i > 1) {
      meas->hlep->Add(mcs[dataset_names[i]]->hlep, -1);
      meas->hhad->Add(mcs[dataset_names[i]]->hhad, -1);
      meas->hcomb->Add(mcs[dataset_names[i]]->hcomb, -1);
    }
  }

  meas->fitHists();
  if (!cfg.noplots) {
    P.SetData(meas->histo, dataset_names[0]);

    P.Plot(string("results/measurement/mass.pdf"), false);
  }
}


void run_ttbaranalysis(Config& cfg) {
  Plotter P;
  std::map<string, TTBarAnalysis*> analyses;
  std::map<string, TChain*> chains;

  size_t i = cfg.blind? 1 : 0;
  for (; i < dataset_files.size(); i++) {
    analyses[dataset_names[i]] = new TTBarAnalysis(cfg);
    chains[dataset_names[i]] = new TChain("events");
    chains[dataset_names[i]]->Add((cfg.data_dir + dataset_files[i]).c_str());
    chains[dataset_names[i]]->Process(analyses[dataset_names[i]]);

    if (i == 0) { // data
      P.SetData(analyses[dataset_names[i]]->histo, dataset_names[i]);
    } else {
      P.AddBg(analyses[dataset_names[i]]->histo, dataset_names[i]);
    }
  }

  // Print logarithmic plots to PDF file "results_log.pdf"
  P.Plot(string("results/results_log.pdf"), true);
  // Print linear plots to PDF file "results_lin.pdf"
  P.Plot(string("results/results_lin.pdf"), false);

  //////////////////////////////////////////////////////////////////////
  // computation of results

  // here you can add the computation of the results, e.g. trigger efficiency,
  // top quark cross-section or top-quark mass. In order to do this, you need
  // to access the histograms in the individual files. This can be done easily
  // and is shown in an example in the next lines.

  // as an example, extract number of events in muon pT histogram in data
  TH1D * h_data_muonpt = (analyses[dataset_names[0]])->histo["Muon_Pt"];
  double NMuonsData = h_data_muonpt->Integral();
  cout << "Found " << NMuonsData << " muons in " << cfg.lumi << "/pb data." << endl;

  //////////////////////////////////////////////////////////////////////
  // saving results to a file

  // the next lines show how you can write individual histograms to a file,
  // with which you can work later.
  TFile f("results/results.root", "RECREATE");
  h_data_muonpt->Write();
  (analyses[dataset_names[0]])->histo["NIsoMuon"]->Write();
  // After writing the histograms, you need to close the file.
  f.Close();

  // you can also save all histograms from one process in a file.
  (analyses[dataset_names[1]])->histo.Write("results/ttbar.root");

}


int main(int argc, char* argv[])
{
  auto cfg = get_config(argc, argv);

  if        (cfg.mode == "Investigation" || cfg.mode == "inv") {
    run_investigation(cfg);
  } else if (cfg.mode == "TTBarAnalysis" || cfg.mode == "ttbar") {
    run_ttbaranalysis(cfg);
  } else if (cfg.mode == "TriggerEfficiency" || cfg.mode == "trigger") {
    run_trigger_efficiency(cfg);
  } else if (cfg.mode == "EventSelection" || cfg.mode == "select"){
    run_event_selection(cfg);
  } else if (cfg.mode == "MassMeasurement" || cfg.mode == "mass") {
    run_mass_measurement(cfg);
  } else if (cfg.mode == "CrossSectionMeasurement" || cfg.mode == "xsec") {
    run_cross_section_measurement(cfg);
  } else {
    std::cout << "No analysis mode provided, specify mode with --mode" << std::endl;
  }

  return 0;
}
