import os
import json

import luigi
import numpy as np
from itertools import product

import common as c
import cfg


PROJECT_PATH = '/home/repo/ttbar'
os.chdir(PROJECT_PATH)


class MassMeasurement(luigi.Task):

    def requires(self):
        return c.BuildAnalysis()

    def output(self):
        return luigi.LocalTarget('results/mass.json')

    def run(self):
        res = {}
        mass_list = []
        mass_lep_list = []
        mass_had_list = []

        settings = cfg.mass('mass')['opts']
        del settings['noplots']
        c.run_task(settings)
        output = c.read_json('results/measurement/mass.json')
        m = float(output['mass'])
        m_lep = float(output['mass_lep'])
        m_had = float(output['mass_had'])

        # statistics
        m_stat = float(output['mass_err'])
        m_lep_stat = float(output['mass_lep_err'])
        m_had_stat = float(output['mass_had_err'])

        # systematics
        for name, bias in product(cfg.variables.keys(), ['lower', 'upper']):
            value = cfg.variables[name]
            error = cfg.errors[name]
            identifier = "%s_%s" % (name, bias)

            settings = cfg.mass(identifier)['opts']
            settings[name] = (value + error) if bias == 'upper' else (value - error)
            if settings[name] < 0:
                settings[name] = 0

            c.run_task(settings)

            output = c.read_json('results/measurement/%s.json' % identifier)

            mass_list.append(float(output['mass']))
            mass_lep_list.append(float(output['mass_lep']))
            mass_had_list.append(float(output['mass_had']))
            # res[identifier] = float(output['mass'])

        res['lep'] = {
            'mass': m_lep,
            'mass_stat': m_lep_stat,
            'mass_sys_upper': max(mass_lep_list) - m_lep,
            'mass_sys_lower': min(mass_lep_list) - m_lep,
        }
        res['had'] = {
            'mass': m_had,
            'mass_stat': m_had_stat,
            'mass_sys_upper': max(mass_had_list) - m_had,
            'mass_sys_lower': min(mass_had_list) - m_had,
        }
        res['total'] = {
            'mass': m,
            'mass_stat': m_stat,
            'mass_sys_upper': max(mass_list) - m,
            'mass_sys_lower': min(mass_list) - m,
        }

        c.write_json('results/mass.json', res)
