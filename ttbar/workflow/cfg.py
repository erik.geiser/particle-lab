

investigation = {
    'opts': {
        'mode': 'Investigation',
    },
    'outputs': ['investigation/plots.pdf', 'investigation/plots_log.pdf',
                'investigation/Z.json'],
}

ttbaranalysis = {
    'opts': {
        'mode': 'TTBarAnalysis',
    },
    'outputs': ['results_lin.pdf', 'results_log.pdf', 'results.root', 'ttbar.root'],
}

trigger_efficiency = {
    'opts': {
        'mode': 'TriggerEfficiency',
    },
    'outputs': ['trigger_efficiency/turnon.pdf'],
}

event_selection = {
    'opts': {
        'mode': 'EventSelection',
    },
    'outputs': ['event_selection/selection_lin.pdf', 'event_selection/selection_log.pdf',
                'event_selection/eventnumbers.json', 'event_selection/signaltonoise.pdf'],
}

efficiency = lambda name: {
    'opts': {
        'mode': 'EventSelection',
        'noplots': '',
        'json_name': '%s.json' % name,
        'output_dir': 'efficiency',
    },
    'outputs': ['event_selection/efficiency/%s.json' % name],
}

xsec = lambda name, efficiency, data_bg, lumi: {
    'opts': {
        'mode': 'CrossSectionMeasurement',
        'mc_efficiency': efficiency,
        'data_bg': int(data_bg),
        'lumi': lumi,
        'json_name': '%s.json' % name,
        'output_dir': 'measurement',
        'noplots': '',
    },
    'outputs': ['results/measurement/%s.json' % name]
}

mass = lambda name: {
    'opts': {
        'mode': 'MassMeasurement',
        'json_name': '%s.json' % name,
        'output_dir': 'measurement',
        'noplots': '',
        'full_met': '',
    },
    'outputs': ['results/measurement/%s.json' % name]
}


variables = {
    'BTagScale': 0.95,
    'jet_scale': 1.0,
    'muon_scale': 1.0,
    'jet_smear': 0.0,
}

errors = {
    'BTagScale': 0.05 * 0.95,
    'jet_scale': 0.03 * 1.0,
    'muon_scale': 0.005 * 1.0,
    'jet_smear': 0.15,
}


tasks = [investigation, trigger_efficiency, event_selection, ttbaranalysis]
