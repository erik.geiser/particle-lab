import luigi
import os
import sys
import subprocess

from crosssection import CrossSectionMeasurement
from mass import MassMeasurement
import common as c
import cfg


PROJECT_PATH = '/home/repo/ttbar'
os.chdir(PROJECT_PATH)


class RunAnalyses(luigi.Task):

    rerun = luigi.parameter.BoolParameter()
    dontrecompile = luigi.parameter.BoolParameter()

    def __init__(self, *args, **kwargs):
        super(RunAnalyses, self).__init__(*args, **kwargs)

        if os.path.exists('bin/ttbar') and not self.dontrecompile:
            os.remove('bin/ttbar')

        if self.rerun:
            subprocess.check_call(['make', 'cleanresults'], stdout=sys.stdout, stderr=sys.stderr)

    def requires(self):
        return [CrossSectionMeasurement(), MassMeasurement()]

    def output(self):
        return luigi.LocalTarget('results/result.json')

    def run(self):
        xsec_data = c.read_json('results/crosssection.json')
        mass_data = c.read_json('results/mass.json')['total']

        for key, val in xsec_data.iteritems():
            xsec_data[key] = abs(float(val))

        for key, val in mass_data.iteritems():
            mass_data[key] = abs(float(val))


        fmt = '%.3f +- %.3f (stat) + %.3f (sys) - %.3f (sys)'
        xsec_str = fmt % (xsec_data['xsec'], xsec_data['xsec_stat'],
                          xsec_data['xsec_sys_upper'], xsec_data['xsec_sys_lower'])
        mass_str = fmt % (mass_data['mass'], mass_data['mass_stat'],
                          mass_data['mass_sys_upper'], mass_data['mass_sys_lower'])

        print('\n\n')
        print('###################################################################################')
        print('                                 FINAL RESULTS                                     ')
        print('Cross Section\t\t= %s' % xsec_str)
        print('Mass\t\t\t= %s' % mass_str)
        print('###################################################################################')
        print('\n\n')

        c.write_json('results/result.json', {
            'Cross Section': xsec_str,
            'Mass': mass_str,
        })
