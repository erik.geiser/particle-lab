import os
import json

import luigi
import numpy as np
from pprint import pprint

import common as c
import cfg


PROJECT_PATH = '/home/repo/ttbar'
os.chdir(PROJECT_PATH)


class CrossSectionMeasurement(luigi.Task):

    def requires(self):
        return [c.BuildAnalysis(), c.Prerequisites(), c.Efficiency()]

    def output(self):
        return luigi.LocalTarget('results/crosssection.json')

    def run(self):
        efficiency_data = c.read_json('results/efficiency/result.json')['total']
        efficiency = float(efficiency_data['efficiency'])
        efficiency_stat = float(efficiency_data['efficiency_stat'])
        upper_efficiency = float(efficiency_data['upper_efficiency'])
        lower_efficiency = float(efficiency_data['lower_efficiency'])

        eventnumbers = c.read_json('results/event_selection/eventnumbers.json')
        Nbg = float(eventnumbers['bg_selected_weighted'])
        lumi = float(eventnumbers['lumi'])

        xsec_list = []

        settings = cfg.xsec('xsec', efficiency, Nbg, lumi)['opts']
        del settings['noplots']
        c.run_task(settings)
        xsec_data = c.read_json('results/measurement/xsec.json')
        xsec = float(xsec_data['xsec'])
        xsec_list.append(xsec_data['xsec_theo_upper'])
        xsec_list.append(xsec_data['xsec_theo_lower'])
        Ndata = float(xsec_data['selected_weighted'])

        xsec_stat = np.sqrt(
            Ndata / (lumi**2 * efficiency**2) +
            Nbg / (lumi**2 * efficiency**2) +
            efficiency_stat**2 * (Ndata - Nbg)**2 / (lumi**2 * efficiency**4)
        )

        c.run_task(cfg.xsec('xsec_efficiency_sys_upper', upper_efficiency, Nbg, lumi)['opts'])
        xsec_list.append(c.read_json('results/measurement/xsec_efficiency_sys_upper.json')['xsec'])
        c.run_task(cfg.xsec('xsec_efficiency_sys_lower', lower_efficiency, Nbg, lumi)['opts'])
        xsec_list.append(c.read_json('results/measurement/xsec_efficiency_sys_lower.json')['xsec'])

        c.run_task(cfg.xsec('xsec_lumi_sys_upper', efficiency, Nbg, lumi + 0.1 * lumi)['opts'])
        xsec_list.append(c.read_json('results/measurement/xsec_lumi_sys_upper.json')['xsec'])
        c.run_task(cfg.xsec('xsec_lumi_sys_lower', efficiency, Nbg, lumi - 0.1 * lumi)['opts'])
        xsec_list.append(c.read_json('results/measurement/xsec_lumi_sys_lower.json')['xsec'])

        xsec_list = [float(xs) for xs in xsec_list]

        res = {
            'xsec': xsec,
            'xsec_stat': xsec_stat,
            'xsec_sys_upper': max(xsec_list) - xsec,
            'xsec_sys_lower': min(xsec_list) - xsec,
            'upper_xsec': max(xsec_list),
            'lower_xsec': min(xsec_list),
        }

        c.write_json('results/crosssection.json', res)
