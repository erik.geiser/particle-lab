"""
Example Luigi Modules
Run analysis with: luigi --module base RunAnalysis --local-scheduler
"""

import os
import sys
import subprocess
import json
from itertools import product
from pprint import pprint

import luigi
import numpy as np

import cfg


PROJECT_PATH = '/home/repo/ttbar'
os.chdir(PROJECT_PATH)


def run_task(settings):
    cmd = ['./bin/ttbar']
    for key, value in settings.iteritems():
        cmd.append('--%s' % key)
        cmd.append(str(value))

    print('\n\n%s\n\n' % ' '.join(cmd))
    subprocess.check_call(cmd, stdout=sys.stdout, stderr=sys.stderr)


def read_json(name):
    with open(name) as data_file:
        data = json.load(data_file)
    return data


def write_json(name, data):
    with open(name, 'w') as data_file:
        print('\n[i] %s:' % name)
        pprint(data)
        print('\n\n')
        json.dump(data, data_file, indent=4)


class BuildAnalysis(luigi.Task):

    def output(self):
        return luigi.LocalTarget("%s/bin/ttbar" % PROJECT_PATH)

    def run(self):
        subprocess.check_call('make', stdout=sys.stdout, stderr=sys.stderr)


class Prerequisites(luigi.Task):

    def requires(self):
        return BuildAnalysis()

    def output(self):
        output_list = []
        for task in cfg.tasks:
            output_list += task['outputs']

        return [
            luigi.LocalTarget('results/%s' % t)
            for t in output_list
        ]

    def run(self):
        for task in cfg.tasks:
            run_task(task['opts'])


class Efficiency(luigi.Task):

    def requires(self):
        return BuildAnalysis()

    def output(self):
        return luigi.LocalTarget('results/efficiency/result.json')

    def run(self):
        efficiencies = {}
        efficiency_list = []

        run_task(cfg.efficiency('efficiency')['opts'])
        output = read_json('results/efficiency/efficiency.json')
        eff = float(output["sgn_selected"]) / float(output["sgn_total"])

        # statistics

        eff_stat = eff * np.sqrt(1 / float(output["sgn_selected_weighted"]) +
                                 1 / float(output["sgn_total_weighted"]))

        # systematics
        for name, bias in product(cfg.variables.keys(), ['lower', 'upper']):
            value = cfg.variables[name]
            error = cfg.errors[name]
            identifier = "%s_%s" % (name, bias)

            settings = cfg.efficiency(identifier)['opts']
            settings[name] = (value + error) if bias == 'upper' else (value - error)

            run_task(settings)

            output = read_json('results/efficiency/%s.json' % identifier)

            selected = float(output["sgn_selected"])
            total = float(output["sgn_total"])
            efficiency = selected / total
            efficiency_list.append(efficiency)

            efficiencies[identifier] = {
                'selected': selected,
                'total': total,
                'efficiency': efficiency,
            }

        efficiencies['total'] = {
            'lower_efficiency': min(efficiency_list),
            'efficiency': eff,
            'efficiency_stat': eff_stat,
            'efficiency_sys_upper': min(efficiency_list) - eff,
            'efficiency_sys_lower': max(efficiency_list) - eff,
            'upper_efficiency': max(efficiency_list),
        }

        write_json('results/efficiency/result.json', efficiencies)
