/*
 * \file EventSelection.C
 *
 */

#include "EventSelection.hpp"
#include "argparse.hpp"

#include <iostream>
#include <algorithm>

#include <TH1D.h>

using namespace std;

EventSelection::EventSelection(Config& _cfg)
  : MyAnalysis()
{
  cfg = _cfg;
  MyJet::SetBTagScaleFactor(cfg.scale_factor);

  cutflow = CreateHisto("cutflow", "cut flow", 11, 0, 11);

  Fill("cutflow", "all", 0);
  Fill("cutflow", "trigger", 0);
  Fill("cutflow", "NJets", 0);
  Fill("cutflow", "BJets", 0);
  Fill("cutflow", "JetPt", 0);
  Fill("cutflow", "pTMu", 0);
  Fill("cutflow", "DrellYan", 0);
  Fill("cutflow", "NIsoMuon", 0);
  Fill("cutflow", "MET", 0);
  Fill("cutflow", "Wreconlep", 0);
  Fill("cutflow", "Wrecon", 0);

  CreateCountHisto("N-1 Njets","N-1 plot for number of jets", 0, 10);
  CreateCountHisto("N-1 Bjets","N-1 plot for number of Bjets", 0, 5);
  CreateHisto("N-1 EtaJets","N-1 plot for EtaJets", 10, -3, 3);
  CreateHisto("N-1 Met","N-1 plot for MET", 20, 0, 300);
  CreateHisto("N-1 pTMu","N-1 plot for pt of Muons", 20, 0, 400);
  CreateHisto("N-1 Drell Yan","N-1 plot for Drell Yan Cut", 20, 0, 400);
  CreateCountHisto("N-1 NIsoMu","N-1 plot for Number of IsoMuons", 0, 10);
  CreateHisto("N-1 Wrecon","N-1 plot for W mass reconstruction", 100, 0, 200);
  CreateHisto("N-1 Wreconlep","N-1 plot for W mass reconstruction leptonic", 100, 0, 200);

  // initialize your variables here
  TotalEvents = 0;
  TotalEventsWeighted = 0;
  SelectedEvents = 0;
  SelectedEventsWeighted = 0;
  SignalToBackground = 0;
}

void EventSelection::CreateHistograms()
{
  // We tell ROOT that we use weighted histograms
  TH1::SetDefaultSumw2();
}

Bool_t EventSelection::Process(Long64_t entry)
{
  GetEntry(entry);
  BuildEvent();

  TotalEvents++;
  TotalEventsWeighted += EventWeight;

  if (TotalEvents % 10000 == 0 && !cfg.quiet)
    cout << "[*] EventSelection at Event: " << TotalEvents << endl;

  //////////////////////////////////////////////////////////////////////
  // implementation of systematic effects

  // As an example, we show how to apply the muon scale uncertainty
  for (unsigned int i = 0; i < Muons.size(); i++) {
    Muons[i] *= cfg.muon_scale;
  }

  //Jet scaling
  for (unsigned int i = 0; i < Jets.size(); i++) {
    Jets[i] *= cfg.jet_scale * (1.0 + cfg.jet_smear);
    if (Jets[i].IsBTagged() && cfg.manual_bscale) {
      Jets[i] *= cfg.BTagScale;
    }
  }

  //////////////////////////////////////////////////////////////////////
  // first, we identify all objects (muons, jets) by applying quality and
  // isolation requirements.

  vector<MyMuon> isomu;
  double hardestIsoMuonPt = 0;
  for (size_t i = 0; i < Muons.size(); i++) {
    if (Muons[i].IsIsolated()) {
      isomu.push_back(Muons[i]);
      if (Muons[i].Pt() > hardestIsoMuonPt) {
        hardestIsoMuonPt = Muons[i].Pt();
      }
    }
  }

  vector<MyJet> rjets;
  vector<MyJet> bjets;
  double hardestJetPt = 0;
  for (size_t i = 0; i < Jets.size(); i++) {
    if (Jets[i].GetJetID()) {
      rjets.push_back(Jets[i]);
      if (Jets[i].IsBTagged()) {
        bjets.push_back(Jets[i]);
      }
      if(rjets[i].Pt() > hardestJetPt){
        hardestJetPt = rjets[i].Pt();
      }
    }
  }

  //////////////////////////////////////////////////////////////////////
  // trigger
  bool triggered = (isomu.size() > 0) && (hardestIsoMuonPt > 26); // above turn on

  //////////////////////////////////////////////////////////////////////
  // Njets
  bool enough_rjets = (rjets.size() > 2);


  //////////////////////////////////////////////////////////////////////
  // Bjets
  bool bjets_present = (bjets.size() > 0);

  //////////////////////////////////////////////////////////////////////
  // JetPt
  bool jet_pt_above = false;
  for (auto& jet : rjets) {
    if (abs(jet.Pt()) > 45) {
      jet_pt_above = true;
      break;
    }
  }

  //////////////////////////////////////////////////////////////////////
  // MET
  bool met_above_threshold = (met.E() > 50);

  // eta muon
  //////////////////////////////////////////////////////////////////////
  // MuonPt
  bool muon_pt_below_threshold = false;
  for (auto& mu : isomu) {
    if (!(mu.Pt() > 125)) {
      muon_pt_below_threshold = true;
      break;
    }
  }

  //////////////////////////////////////////////////////////////////////
  // Drell Yan reduction
  bool muon_from_z = !(isomu.size() == 2 && (isomu[0] + isomu[1]).M() >= 86 &&
                     (isomu[0] + isomu[1]).M() <= 140);

  //////////////////////////////////////////////////////////////////////
  // Reconstructed W leptons
  bool w_from_leptons = false;
  if (isomu.size() == 1 ) {
    if ((isomu[0]+met).M() > 82 || (isomu[0]+met).M() < 78) {
      w_from_leptons = true;
    }
  }

  //////////////////////////////////////////////////////////////////////
  // Wmass
  bool Wreconstruct_at_least = false;
  vector<MyJet> Wreconstruction;
  double Wjet=0;
  for (auto& jet : rjets) {
    if(!jet.IsBTagged()){
      Wreconstruction.push_back(jet);
    }
  }
  if (Wreconstruction.size() == 2) {
    for (unsigned int i = 0; i < Wreconstruction.size(); i++) {
      Wjet += Wreconstruction[i].E();
    }
    if (Wjet > 80) {
      Wreconstruct_at_least = true;
    }
  }

  //////////////////////////////////////////////////////////////////////
  // NIsoMu
  bool enough_iso_muons = (isomu.size() == 1);


  if (!cfg.exclude_prior_selection) Fill("cutflow", "all");
  if (triggered){
    if (!cfg.exclude_prior_selection) Fill("cutflow", "trigger");
    if (enough_rjets){
      Fill("cutflow", "NJets");
      if (bjets_present){
        Fill("cutflow", "BJets");
        if (jet_pt_above){
          Fill("cutflow", "JetPt");
          if (muon_pt_below_threshold){
            Fill("cutflow", "pTMu");
            if (muon_from_z){
              Fill("cutflow", "DrellYan");
              if (enough_iso_muons){
                Fill("cutflow","NIsoMuon");
                SelectedEvents++;
                SelectedEventsWeighted += EventWeight;
                if (!met_above_threshold){
                  Fill("cutflow", "MET");
                  if (!w_from_leptons){
                    Fill("cutflow","Wreconlep");
                    if (Wreconstruct_at_least){
                      Fill("cutflow", "Wrecon");
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }


  //////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////
  //N-1 plots
/*
  //Njets
  if(bjets_present&&muon_from_z&&muon_pt_below_threshold&&!met_above_threshold &&jet_eta_below_threshold&&w_from_leptons ) {
    FillCount("N-1 Njets", rjets.size());
  }

  //Bjets
  if( enough_rjets &&muon_from_z&&muon_pt_below_threshold&&!met_above_threshold &&jet_eta_below_threshold&&w_from_leptons ) {
    FillCount("N-1 Bjets", bjets.size());
  }

  //Met
  if( enough_rjets && bjets_present&&muon_from_z&&muon_pt_below_threshold&&jet_eta_below_threshold&&w_from_leptons ) {
    Fill("N-1 Met", met.E());
  }

  //EtaJets
  if( enough_rjets && bjets_present&&muon_from_z&&muon_pt_below_threshold&&!met_above_threshold&&w_from_leptons  ) {
    for (auto& jet : rjets) {
      Fill("N-1 EtaJets", jet.Eta());
    }
  }

  //ptMu
  if (enough_rjets && bjets_present && muon_from_z &&!met_above_threshold &&jet_eta_below_threshold&&w_from_leptons ) {
    for (auto& mu : isomu) {
      Fill("N-1 pTMu", mu.Pt());
    }
  }

  // Reconstructed W leptons
  if (enough_rjets && bjets_present && muon_from_z && muon_pt_below_threshold &&!met_above_threshold &&jet_eta_below_threshold) {
    Fill("N-1 Wreconlep", met.E());
  }


  //DrellYan
  if (enough_rjets && bjets_present && muon_pt_below_threshold &&!met_above_threshold &&jet_eta_below_threshold&&w_from_leptons ) {
    Fill("N-1 Drell Yan", (isomu[0] + isomu[1]).E());
  }

  //NIsoMu
  if (enough_rjets && bjets_present && muon_from_z && muon_pt_below_threshold &&!met_above_threshold &&jet_eta_below_threshold&&w_from_leptons) {
    FillCount("N-1 NIsoMu", isomu.size());
  }

*/

  return kTRUE;
}
