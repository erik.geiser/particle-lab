/*
 * \file TriggerEfficiency.h
 *
 */
#ifndef TriggerEfficiency_hpp
#define TriggerEfficiency_hpp

#include "MyAnalysis.hpp"
#include "argparse.hpp"
#include <string>
#include <TGraphAsymmErrors.h>


class TriggerEfficiency: public MyAnalysis {
public:
  TriggerEfficiency(Config& _cfg);
  virtual ~TriggerEfficiency() { };

  virtual void CreateHistograms();
  virtual Bool_t Process(Long64_t entry);
  void finalize();

  TH1D* total;
  TH1D* triggered;
  int TotalEvents;
  Config cfg;
};

#endif
