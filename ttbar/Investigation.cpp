/*
 * \file Investigation.C
 *
 */

#include "Investigation.hpp"
#include "argparse.hpp"
#include "TF1.h"
#include "tools.hpp"

#include <iostream>
#include <string>
#include <algorithm>
#include <cmath>
#include <map>

#include <TH1D.h>

using namespace std;

Investigation::Investigation(Config& _cfg)
  : MyAnalysis()
{
  cfg = cfg;
  MyJet::SetBTagScaleFactor(cfg.scale_factor);

  // initialize your variables here
  TotalEvents = 0;
  SelectedEvents = 0;
}

void Investigation::CreateHistograms()
{
  // We tell ROOT that we use weighted histograms
  TH1::SetDefaultSumw2();

  CreateCountHisto("Njets", "Number of Jets", 0, 11);
  CreateCountHisto("Nbjets", "Number of b-tagged jets", 0, 6);
  CreateCountHisto("Nrjets", "Number of reduced jets", 0, 11);
  CreateCountHisto("NIsoMuon", "Number of isolated muons", 0, 4);
  CreateHisto("Eta", "Pseudorapidity of jets", 20, 0, 3);
  CreateHisto("Muon_Pt","p_{T} of isolated muons", 15, 0, 700);
  CreateHisto("HardestIsoPt", "p_{T} of the hardest isolated muon", 15, 0, 700);
  CreateHisto("EtaMuon","Pseudorapidity of isolated muons", 20, 0, 3);
  CreateHisto("Jet_Pt","p_{T} of reduced Jets", 16, 0, 800);
  CreateHisto("HardestJetPt", "p_{T} of the hardest jet", 16, 0, 800);
  CreateHisto("MET","Missing transversal energy", 50, 0, 400);
  //CreateHisto("ReconstructedZMass","Mass of a Z Boson in Drell Yan",110,40,150);
  h = CreateHisto("ReconstructedZMass","Mass of a Z Boson in Drell Yan", 80, 75, 105);
}

Bool_t Investigation::Process(Long64_t entry)
{
  GetEntry(entry);
  BuildEvent();

  ++TotalEvents;

  if (TotalEvents % 10000 == 0 && !cfg.quiet) {
    cout << "[*] Investigation at Event:" << TotalEvents << endl;
  }




  //////////////////////////////////////////////////////////////////////
  // implementation of systematic effects

  for (unsigned int i = 0; i < Muons.size(); i++) {
    Muons[i] *= cfg.muon_scale;
  }
	for (unsigned int i = 0; i < Jets.size(); i++) {
    Jets[i] *= cfg.jet_scale * (1.0 + cfg.jet_smear);
    if (Jets[i].IsBTagged() && cfg.manual_bscale) {
      Jets[i] *= cfg.BTagScale;
    }
  }



  //////////////////////////////////////////////////////////////////////
  // Setup

  // Muon isolation
  vector<MyMuon> isomu;
  double hardestIsoMuonPt = 0;
  for (unsigned int i = 0; i < Muons.size(); i++) {
    if (Muons[i].IsIsolated()) {
      isomu.push_back(Muons[i]);
      Fill("Muon_Pt", Muons[i].Pt());
      Fill("EtaMuon", Muons[i].Eta());
      if (Muons[i].Pt() > hardestIsoMuonPt) hardestIsoMuonPt = Muons[i].Pt();
    }
  }


  vector<MyJet> BJets;
	vector<MyJet> reducedJets;
  double hardestJetPt = 0;
	for (unsigned int i = 0; i < Jets.size(); i++) {
		if (Jets[i].GetJetID() ){
			if (Jets[i].IsBTagged() ){
				BJets.push_back(Jets[i]);
			}
      //Testing if all Jets are good
      //std::cout << "## GetJetID" << Jets[i].GetJetID() << std::endl;
			reducedJets.push_back(Jets[i]);
      if (Jets[i].Pt() > hardestJetPt) hardestJetPt = Jets[i].Pt();
      Fill("Jet_Pt", Jets[i].Pt());
      Fill("Eta", Jets[i].Eta());
		}
	}

  if (isomu.size() == 0 || hardestIsoMuonPt < 40) return kFALSE;
  //////////////////////////////////////////////////////////////////////
  // Investigation plots


  Fill("MET",met.Pt());
  Fill("HardestIsoPt", hardestIsoMuonPt);
  Fill("HardestJetPt", hardestIsoMuonPt);
  FillCount("Njets", Jets.size());
  FillCount("Nbjets", BJets.size());
  FillCount("Nrjets", reducedJets.size());
  FillCount("NIsoMuon", isomu.size());

  //////////////////////////////////////////////////////////////////////
  // Investigation for Drell Yan peak

  vector<MyMuon> DY_Muons;

  double reconstructedZMass = 0;
  double energySum = 0;

  if(isomu.size() == 2){
    for (unsigned int i = 0; i < Muons.size(); i++){
      energySum += Muons[i].E();
    }
    if (energySum >= 26){
      DY_Muons.push_back(Muons[0]);
      DY_Muons.push_back(Muons[1]);
    }
  reconstructedZMass = (DY_Muons[0] + DY_Muons[1]).M();
  }
  Fill("ReconstructedZMass",reconstructedZMass);



  return kTRUE;
}


void Investigation::HistFit(){

  TF1 * BW = new TF1("BW","[2]/((x^2-[1]^2)^2+[1]^2*[0]^2)",75,105);
  BW->SetParameter(0,5.0);
  BW->SetParameter(1,20.0);
  BW->SetParameter(2,91.0);

  TFitResultPtr rootfit = h->Fit("BW", "QRS");

  std::map<std::string, double> p;
  p["gamma"] = BW->GetParameter(0);
  p["m"] = BW->GetParameter(1);
  p["const"] = BW->GetParameter(2);
  p["gamma_err"] = BW->GetParError(0);
  p["m_err"] = BW->GetParError(1);
  p["const_err"] = BW->GetParError(2);


  save_json("results/investigation/Z.json", p);
}
