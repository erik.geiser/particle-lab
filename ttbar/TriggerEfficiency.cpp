/*
 * \file TriggerEfficiency.C
 *
 */

#include "TriggerEfficiency.hpp"
#include "argparse.hpp"
#include "tools.hpp"

#include <iostream>
#include <string>
#include <algorithm>

#include <TH1D.h>
#include <TGraphAsymmErrors.h>
#include <TCanvas.h>


using namespace std;

TriggerEfficiency::TriggerEfficiency(Config& _cfg)
  : MyAnalysis()
{
  cfg = _cfg;
  MyJet::SetBTagScaleFactor(cfg.scale_factor);
  // setRootStyle();
  // initialize your variables here
  TotalEvents = 0;
}

void TriggerEfficiency::CreateHistograms()
{
  // We tell ROOT that we use weighted histograms
  TH1::SetDefaultSumw2();

  total = new TH1D("total", "total", 20, 0, 100);
  triggered = new TH1D("triggered", "triggered", 20, 0, 100);
}

Bool_t TriggerEfficiency::Process(Long64_t entry)
{
  GetEntry(entry);
  BuildEvent();

  ++TotalEvents;

  if (TotalEvents % 10000 == 0 && !cfg.quiet) {
    cout << "[*] TriggerEfficiency at Event:" << TotalEvents << endl;
  }

  //////////////////////////////////////////////////////////////////////
  // implementation of systematic effects

  for (unsigned int i = 0; i < Muons.size(); i++) {
    Muons[i] *= cfg.muon_scale;
  }
	for (unsigned int i = 0; i < Jets.size(); i++) {
    Jets[i] *= cfg.jet_scale;
  }

  //////////////////////////////////////////////////////////////////////
  // Trigger Efficiency Calculation

  vector<MyMuon> isomu;
  for (size_t i = 0; i < Muons.size(); i++) {
    if (Muons[i].IsIsolated()) {
      isomu.push_back(Muons[i]);
    }
  }

  if (isomu.size() == 1) {
    total->Fill(isomu[0].Pt());
    if (triggerIsoMu24) triggered->Fill(isomu[0].Pt());
  }

  return kTRUE;
}


void TriggerEfficiency::finalize() {
  TGraphAsymmErrors* turnOn = new TGraphAsymmErrors(triggered, total, "cp");
  turnOn->GetXaxis()->SetTitle("p_{T}");
  turnOn->GetYaxis()->SetTitle("Efficiency");
  turnOn->SetTitle("Trigger Turn On");

  TCanvas c;
  turnOn->Draw();
  c.Print("results/trigger_efficiency/turnon.pdf");
}
