from itertools import product
from glob import glob
from collections import namedtuple
import os

import luigi
import numpy as np
import matplotlib.pyplot as plt
from scipy.odr import RealData, Model, ODR

from data.muon_setup import efficiency_data
import muon_helpers as h


def linear_kink((V0, m1, m2, const), V):
    return const + (V - V0) * (m1 + m2 * np.tanh(V - V0))


def linear((m, y0), x):
    return m * x + y0


def gaussian((c, mu, sigma), x):
    return c * np.exp(-(x - mu)**2 / (2. * sigma**2))


class Efficiency(luigi.Task):

    _rounds = (1, 2)
    _pmts = (1, 2, 3, 4)

    @property
    def rounds(self):
        return ['rnd%d' % i for i in self._rounds]

    @property
    def pmts(self):
        return self._pmts

    def output(self):
        outputs = {
            '%s%d' % (r, p): h.plot_target('eff_%s_pmt%d.pdf' % (r, p), folder='efficiency')
            for r, p in product(self.rounds, self.pmts)
        }
        outputs['data'] = h.data_target('efficiencies.json', folder='processed')
        return outputs

    def run(self):
        h.assure_path('plots/efficiency', 'data/processed')
        efficiencies = {}

        for i, (rnd, pmt) in enumerate(product(self.rounds, self.pmts)):
            data = efficiency_data[rnd][pmt]
            V = data['V']
            V_err = efficiency_data['V_error'] * np.ones_like(V)
            # incorporate overflow error in N1 measurement due to shitty counter
            N1_rel_err = (np.sqrt(data['N1'] + data['scale_error']**2) /
                          data['N1'].astype(np.float64))

            e = data['N4'] / data['N3'].astype(np.float64)
            e_err = e * (np.sqrt(data['N4'])**-1 + np.sqrt(data['N3'])**-1)
            p = data['N4'] / data['N1'].astype(np.float64)
            p_err = p * (np.sqrt(data['N4'])**-1 + N1_rel_err)

            e_thresh = 0.02
            fit = ODR(RealData(V[e > e_thresh], e[e > e_thresh], sx=V_err, sy=e_err),
                      Model(linear_kink), beta0=(2000, 0.04, 0.02, 0)).run()
            if fit.stopreason[0] != 'Sum of squares convergence':
                print('[!] Fit for PMT%d (%s) did not converge!' % (pmt, rnd))
            Vfit = np.linspace(1700, 2400, 200)
            efit = linear_kink(fit.beta, Vfit)

            fig, ax = plt.subplots()
            ax.plot(Vfit, efit, '--', color='dimgrey', label='Fit')
            ax.errorbar(V, e, xerr=V_err, yerr=e_err, fmt='.', label='Efficiency', capsize=1.4)
            ax.errorbar(V, p, xerr=V_err, yerr=p_err, fmt='.', label='Purity', capsize=1.4)
            ax.axvline(fit.beta[0], color='red',
                       label='$V_0 = %.1f\pm %.1f \mathrm{V}$' % (fit.beta[0], fit.sd_beta[0]))
            ax.legend(loc='center right')
            ax.grid()
            ax.set_xlabel('Voltage [V]')
            ax.set_xlim(1700, 2400)
            ax.set_yticks(np.arange(0, 1.1, 0.1))
            ax.set_ylim(0, 1.)
            ax.set_title('PMT %d,  Round %s' % (pmt, rnd[-1]))
            fig.savefig(self.output()['%s%d' % (rnd, pmt)].path)
            plt.close(fig)

            efficiencies['%s_pmt%s' % (rnd, pmt)] = linear_kink(fit.beta, fit.beta[0])

        h.write_json(self.output()['data'].path, efficiencies)


class TimeDataProcessing(luigi.Task):

    delay_readout_error = 5  # ns
    plot_window = 30

    def output(self):
        output = {'data': h.data_target('time_calib.npy', folder='processed')}
        for delay in h.calib_files.keys():
            output[delay] = h.plot_target('%s.pdf' % delay, folder='time_calibration')
        return output

    def run(self):
        h.assure_path('data/processed', 'plots/time_calibration')
        times, times_err, channels, channels_err = [], [], [], []

        for delay, target in h.calib_files.iteritems():
            counts = np.loadtxt(target.path, skiprows=2).flatten()
            counts[counts == 0] = 1e-8
            counts_err = np.sqrt(counts)
            chans = np.arange(counts.size)
            chans_err = np.sqrt(12)**-1 * np.ones_like(chans)

            expected_peak = np.argmax(counts)
            selector = ((chans > (expected_peak) - 5) & (chans < (expected_peak + 5)))
            beta0 = (np.max(counts), expected_peak, 1)
            fit = ODR(RealData(chans[selector], counts[selector],
                               sx=chans_err[selector], sy=counts_err[selector]), Model(gaussian),
                      beta0=beta0).run()
            if fit.stopreason[0] != 'Sum of squares convergence':
                print('[!] Fit for %sns did not converge!' % delay)
            fit_chan = np.linspace(expected_peak - self.plot_window,
                                   expected_peak + self.plot_window,
                                   200)
            fit_count = gaussian(fit.beta, fit_chan)

            print("Delay: %dns" % delay)
            print("  -> c0: %.2f+-%.2f, x0: %.2f+-%.2f, sigma: %.2f+-%.2f" %
                  (fit.beta[0], fit.sd_beta[0], fit.beta[1], fit.sd_beta[1],
                   fit.beta[2], fit.sd_beta[2]))

            fig, ax = plt.subplots()
            ax.plot(fit_chan, fit_count, label='Gaussian Fit')
            ax.errorbar(chans, counts, xerr=chans_err, yerr=counts_err, fmt='x', capsize=1)
            ax.set_xlim(expected_peak - self.plot_window, expected_peak + self.plot_window)
            ax.set_xlabel('Channel Nr.')
            ax.set_ylabel('Counts')
            ax.set_title('Delay: %dns' % delay)
            ax.legend()
            fig.savefig(self.output()[delay].path)
            plt.close(fig)

            times.append(delay)
            times_err.append(self.delay_readout_error)
            channels.append(fit.beta[1])
            channels_err.append(fit.sd_beta[1])

        data = np.empty((4, len(times)))
        data[0] = times
        data[1] = channels
        data[2] = times_err
        data[3] = channels_err
        np.save(self.output()['data'].path, data)


class TimeCalibration(luigi.Task):

    def requires(self):
        return TimeDataProcessing()

    def output(self):
        return {
            'fit': h.plot_target('fit.pdf', folder='time_calibration'),
            'fitdata': h.data_target('time_calib_fit.json', folder='processed')
        }

    def run(self):
        t, c, t_err, c_err = np.load(self.input()['data'].path)

        fit = ODR(RealData(c, t, sx=c_err, sy=t_err), Model(linear), beta0=(1, 1)).run()
        fit_c = np.linspace(np.min(c), np.max(c), 200)
        fit_t = linear(fit.beta, fit_c)

        fig, (ax, rax) = plt.subplots(2, sharex=True, gridspec_kw={'height_ratios': [3, 1],
                                                                   'hspace': 0})
        ax.plot(fit_c, fit_t, label='Fit: ($%.2f\\cdot N_{chan} + %.2f$) ns' % tuple(fit.beta),
                color='dimgrey')
        ax.errorbar(c, t, yerr=t_err, fmt='x', capsize=1.4, color='darkorange')
        rax.axhline(0, color='dimgrey')
        rax.errorbar(c, t - linear(fit.beta, c), color='darkorange', fmt='x', capsize=1.4,
                     yerr=np.sqrt(t_err**2 + linear(fit.beta, c_err)**2))
        rax.set_xlabel('Channel Nr.')
        ax.set_ylabel('t [ns]')
        rax.set_ylabel('Residual [ns]')
        ax.legend(title='$\chi^2/ndf = %.2f$' % (fit.res_var / (len(c) - 2)))
        fig.savefig(self.output()['fit'].path)
        plt.close(fig)

        h.write_json(self.output()['fitdata'].path, {
            'm': fit.beta[0],
            'm_err': fit.sd_beta[0],
            'y0': fit.beta[1],
            'y0_err': fit.sd_beta[1],
            'chi2': fit.res_var,
            'ndf': len(c) - 2,
            'chi2/ndf': fit.res_var / (len(c) - 2),
        })


class Rebinning(luigi.Task):

    nbins = 60

    def requires(self):
        return TimeCalibration()

    def output(self):
        return {
            'raw_chan': h.data_target('data.npy', folder='processed'),
            'raw_t': h.data_target('data_t.npy', folder='processed'),
            'rebinned': h.data_target('rebinned_data.npy', folder='processed'),
            't_up': h.data_target('rebinned_data_tup.npy', folder='processed'),
            't_down': h.data_target('rebinned_data_tdown.npy', folder='processed'),
            'bins_up': h.data_target('rebinned_data_binsup.npy', folder='processed'),
            'bins_down': h.data_target('rebinned_data_binsdown.npy', folder='processed')
        }

    def rebin(self, ts, counts, delta_nbins=0):
        n_bins = self.nbins + delta_nbins
        hist, bins = np.histogram(ts, bins=n_bins, weights=counts)
        errors = np.sqrt(hist)
        width = (bins[1] - bins[0])
        center = (bins[:-1] + bins[1:]) / 2.0
        Hist = namedtuple('Histogram', ('bins', 'counts', 'errors', 'width'))
        return Hist(center, hist, errors, width)

    def expand(self, hist_tuple):
        return np.array([hist_tuple.bins, hist_tuple.counts, hist_tuple.errors, hist_tuple.width])

    def run(self):
        calib = h.read_json(self.input()['fitdata'].path)
        counts = np.loadtxt(h.full_path('data', 'measurement', 'longmeas.TKA'),
                            skiprows=2).flatten()
        chans = np.arange(counts.size)
        ts = calib['m'] * chans + calib['y0']
        ts_sys = np.sqrt((chans * calib['m_err'])**2 + calib['y0_err']**2)

        rebinned = self.rebin(ts, counts)
        rebinned_tup = self.rebin(ts + ts_sys, counts)
        rebinned_tdown = self.rebin(ts - ts_sys, counts)
        rebinned_binsup = self.rebin(ts, counts, delta_nbins=5)
        rebinned_binsdown = self.rebin(ts, counts, delta_nbins=-5)

        np.save(self.output()['raw_chan'].path, np.array([chans, counts]))
        np.save(self.output()['raw_t'].path, np.array([ts, counts]))
        np.save(self.output()['rebinned'].path, self.expand(rebinned))
        np.save(self.output()['t_up'].path, self.expand(rebinned_tup))
        np.save(self.output()['t_down'].path, self.expand(rebinned_tdown))
        np.save(self.output()['bins_up'].path, self.expand(rebinned_binsup))
        np.save(self.output()['bins_down'].path, self.expand(rebinned_binsdown))
