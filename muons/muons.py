import os

import luigi
import numpy as np
import matplotlib.pyplot as plt
from scipy.odr import RealData, Model, ODR

from muon_calibration import Rebinning, Efficiency
import muon_helpers as h


local_fitranges = {
    'default': {
        'l-': (1000, 4000),
        'l+': (7800, 11000),
        'c': (14000, 20500),
    },
    'range_up': {
        'l-': (1200, 4200),
        'l+': (7700, 10900),
        'c': (14200, 20700),
    },
    'range_down': {
        'l-': (800, 3800),
        'l+': (7900, 11100),
        'c': (13800, 20200),
    }
}


global_fitrange = (0, 20500)


class DecayPlots(luigi.Task):

    def requires(self):
        return Rebinning()

    def output(self):
        return {
            'lin': h.plot_target('decay_lin.pdf', folder='decay'),
            'log': h.plot_target('decay_log.pdf', folder='decay'),
            'lin_raw': h.plot_target('decay_lin_raw.pdf', folder='decay'),
            'log_raw': h.plot_target('decay_log_raw.pdf', folder='decay'),
            'lin_raw_t': h.plot_target('decay_lin_raw_t.pdf', folder='decay'),
            'log_raw_t': h.plot_target('decay_log_raw_t.pdf', folder='decay')
        }

    def run(self):
        h.assure_path('plots/decay')

        raw_ts, raw_counts_t = np.load(self.input()['raw_t'].path)
        raw_chan, raw_counts_chan = np.load(self.input()['raw_chan'].path)
        ts, counts, counts_stat, width = np.load(self.input()['rebinned'].path)
        fitranges = local_fitranges['default']

        fig, ax = plt.subplots()
        ax.set_xlabel('t [ns]')
        ax.set_ylabel('Counts')
        ax.bar(ts, counts, align='center', width=1.1 * width, color='blue', yerr=counts_stat)
        ax.axvspan(*fitranges['l-'], color='red', alpha=0.2, label='First Sector')
        ax.axvspan(*fitranges['l+'], color='green', alpha=0.2, label='Second Sector')
        ax.axvspan(*fitranges['c'], color='darkorange', alpha=0.2, label='Third Sector')
        ax.legend()
        fig.savefig(self.output()['lin'].path)
        plt.close(fig)

        fig, ax = plt.subplots()
        ax.set_xlabel('t [ns]')
        ax.set_ylabel('Counts')
        ax.bar(ts, counts, align='center', width=1.1 * width,
               color='blue', yerr=counts_stat, log=True)
        ax.axvspan(*fitranges['l-'], color='red', alpha=0.2, label='First Sector')
        ax.axvspan(*fitranges['l+'], color='green', alpha=0.2, label='Second Sector')
        ax.axvspan(*fitranges['c'], color='darkorange', alpha=0.2, label='Third Sector')
        ax.legend()
        fig.savefig(self.output()['log'].path)
        plt.close(fig)

        fig, ax = plt.subplots()
        ax.plot(raw_chan, raw_counts_chan, linewidth=.5)
        ax.set_xlabel('Channel')
        ax.set_ylabel('Counts')
        ax.set_ylim(bottom=0, top=50)
        fig.savefig(self.output()['lin_raw'].path)
        ax.set_ylim(top=None)
        ax.set_yscale('symlog')
        fig.savefig(self.output()['log_raw'].path)
        plt.close(fig)

        fig, ax = plt.subplots()
        ax.plot(raw_ts, raw_counts_t, linewidth=.5)
        ax.set_xlabel('t [ns]')
        ax.set_ylabel('Counts')
        ax.set_ylim(bottom=0, top=50)
        fig.savefig(self.output()['lin_raw_t'].path)
        ax.set_ylim(top=None)
        ax.set_yscale('symlog')
        fig.savefig(self.output()['log_raw_t'].path)
        plt.close(fig)


class LocalFits(luigi.Task):

    sys = luigi.ChoiceParameter(
        choices=['', 't_up', 't_down', 'range_up', 'range_down', 'bins_up', 'bins_down'],
        var_type=str, default=''
    )

    def requires(self):
        return Rebinning()

    def output(self):
        pf = 'fit' + ('' if self.sys == '' else '/systematics')
        df = 'processed' + ('' if self.sys == '' else '/systematics')
        return {
            'img_c': h.plot_target('%s.pdf' % '_'.join(('local_c', self.sys)), folder=pf),
            'img_l+': h.plot_target('%s.pdf' % '_'.join(('local_+', self.sys)), folder=pf),
            'img_l-': h.plot_target('%s.pdf' % '_'.join(('local_-', self.sys)), folder=pf),
            'res_c': h.plot_target('%s.pdf' % '_'.join(('local_res_c', self.sys)), folder=pf),
            'res_l+': h.plot_target('%s.pdf' % '_'.join(('local_res_+', self.sys)), folder=pf),
            'res_l-': h.plot_target('%s.pdf' % '_'.join(('local_res_-', self.sys)), folder=pf),
            'pull_c': h.plot_target('%s.pdf' % '_'.join(('local_pull_c', self.sys)), folder=pf),
            'pull_l+': h.plot_target('%s.pdf' % '_'.join(('local_pull_+', self.sys)), folder=pf),
            'pull_l-': h.plot_target('%s.pdf' % '_'.join(('local_pull_-', self.sys)), folder=pf),
            'data': h.data_target('%s.json' % '_'.join(('local', self.sys)), folder=df)
        }

    def run(self):
        h.assure_path(
            'plots/fit' + ('' if self.sys == '' else '/systematics'),
            'data/processed' + ('' if self.sys == '' else '/systematics')
        )
        input_relatet_systematics = ['t_up', 't_down', 'bins_up', 'bins_down']
        input_name = 'rebinned' if self.sys not in input_relatet_systematics else self.sys
        fitrange_name = self.sys if 'range' in self.sys else 'default'
        fit_data = {}

        ts, counts, counts_stat, width = np.load(self.input()[input_name].path)

        for func, err, beta0, name, sector in zip([h.decay_const, h.decay_local, h.decay_local],
                                                  [h.error_const, h.error_local, h.error_local],
                                                  [(1,), (0.0005, 100), (0.0005, 1)],
                                                  ['c', 'l+', 'l-'],
                                                  ['Third', 'Second', 'First']):
            fitrange = local_fitranges[fitrange_name][name]
            selector = (ts > fitrange[0]) & (ts < fitrange[1])
            _ts, _counts, _counts_stat = ts[selector], counts[selector], counts_stat[selector]
            _counts_stat = _counts_stat / _counts
            _counts = h.symlog(_counts)

            fit = ODR(RealData(_ts, _counts, sx=None, sy=_counts_stat),
                      Model(func), beta0=beta0).run()
            fit_t = np.linspace(np.min(_ts), np.max(_ts), 200)
            fit_c = func(fit.beta, fit_t)
            pulls = (_counts - func(fit.beta, _ts)) / _counts_stat
            chi2 = np.sum(pulls**2)

            fig, (ax, rax) = plt.subplots(2, sharex=True, gridspec_kw={'height_ratios': [3, 1],
                                                                       'hspace': 0})
            ax.errorbar(_ts, _counts, yerr=_counts_stat, fmt='.', color='blue', label='Data',
                        capsize=1.4)
            ax.plot(fit_t, fit_c, color='red', label='%s Sector Fit' % sector)
            rax.axhspan(ymin=-1, ymax=1, color='red', alpha=0.1)
            rax.plot(_ts, pulls, '.', color='blue')
            rax.set_xlabel('t [ns]')
            prefix = '' if name == 'c' else 'Reduced '
            ax.set_ylabel(prefix + 'log(Counts)')
            rax.set_ylabel('Pulls')
            ax.legend(title='$\chi^2/ndf = %.2f$' % (chi2 / (len(_ts) - len(fit.beta))))
            fig.savefig(self.output()['img_' + name].path)
            plt.close(fig)

            fig, ax = plt.subplots()
            ax.hist(pulls, bins='auto', range=(-3, 3))
            ax.set_xlabel('Pull Distribution')
            fig.savefig(self.output()['pull_' + name].path)
            plt.close(fig)

            fit_data[name + '_chi2/ndf'] = chi2 / (len(_ts) - len(fit.beta))
            if name == 'c':
                fit_data[name] = fit.beta[0]
                fit_data[name + '_err'] = fit.sd_beta[0]
            else:
                fit_data[name] = fit.beta[0]
                fit_data[name + '_err'] = fit.sd_beta[0]
                fit_data[name + '_const'] = fit.beta[1]
                fit_data[name + '_const_err'] = fit.sd_beta[1]

            fig, ax = plt.subplots()
            label = {
                'c': 'Initial Spectrum',
                'l+': 'Spectrum after\nBackground Correction',
                'l-': 'Spectrum without\nBackground and $\mu_{+}$',
            }[name]
            ax.plot(ts, counts, 'x', color='blue', label=label)
            ax.axvspan(*fitrange, label='Fitrange', color='red', alpha=0.2)
            ax.legend()
            ax.set_xlabel('$t [ns]$')
            ax.set_ylabel(prefix + 'Counts')
            ax.set_yscale('symlog')
            fig.savefig(self.output()['res_' + name].path)
            plt.close(fig)

            counts -= np.exp(func(fit.beta, ts))
            # counts_stat = np.sqrt(counts_stat**2 + err(fit.sd_beta, ts)**2)

        h.write_json(self.output()['data'].path, fit_data)


class GlobalFit(luigi.Task):

    sys = luigi.ChoiceParameter(
        choices=['', 't_up', 't_down', 'bins_up', 'bins_down'],
        var_type=str, default=''
    )
    beta0 = (0.00045, 0.00045, 5000, 5000, 600)

    def requires(self):
        return Rebinning()

    def output(self):
        pf = 'fit' + ('' if self.sys == '' else '/systematics')
        df = 'processed' + ('' if self.sys == '' else '/systematics')
        return {
            'img': h.plot_target('%s.pdf' % '-'.join(('global', self.sys)), folder=pf),
            'pull': h.plot_target('%s.pdf' % '-'.join(('global_pull', self.sys)), folder=pf),
            'data': h.data_target('%s.json' % '-'.join(('global', self.sys)), folder=df)
        }

    def run(self):
        h.assure_path(
            'plots/fit' + ('' if self.sys == '' else '/systematics'),
            'data/processed' + ('' if self.sys == '' else '/systematics')
        )
        input_relatet_systematics = ['t_up', 't_down', 'bins_up', 'bins_down']
        input_name = 'rebinned' if self.sys not in input_relatet_systematics else self.sys

        ts, counts, counts_stat, width = np.load(self.input()[input_name].path)

        selector = ((ts > global_fitrange[0]) & (ts < global_fitrange[1]))
        _ts, _counts, _counts_stat = ts[selector], counts[selector], counts_stat[selector]

        fit = ODR(RealData(_ts, _counts, sx=None, sy=_counts_stat),
                  Model(h.decay_global), beta0=self.beta0).run()
        fit_t = np.linspace(np.min(_ts), np.max(_ts), 200)
        fit_c = h.decay_global(fit.beta, fit_t)
        pulls = (_counts - h.decay_global(fit.beta, _ts)) / _counts_stat
        chi2 = np.sum(pulls**2)

        fig, (ax, rax) = plt.subplots(2, sharex=True, gridspec_kw={'height_ratios': [3, 1],
                                                                   'hspace': 0})
        ax.errorbar(ts, counts, yerr=counts_stat, fmt='.', color='blue', capsize=1.4, label='Data')
        ax.plot(fit_t, fit_c, color='red', label='Global Fit')
        rax.axhspan(-1, 1, color='red', alpha=0.2)
        rax.errorbar(_ts, pulls,
                     color='blue', fmt='x', capsize=1.4)
        rax.set_xlabel('t [ns]')
        rax.set_ylabel('Pulls')
        ax.set_ylabel('Counts')
        ax.legend(title='$\chi^2/ndf = %.2f$' % (chi2 / (len(_ts) - len(fit.beta))))
        fig.savefig(self.output()['img'].path)
        plt.close(fig)

        fig, ax = plt.subplots()
        ax.hist(pulls, bins=10, range=(-4, 4))
        ax.set_xlabel('Pull Distribution')
        fig.savefig(self.output()['pull'].path)
        plt.close(fig)

        lambda_first, lambda_second, c1, c2, c3 = fit.beta
        lambda_first_err, lambda_second_err, c1_err, c2_err, c3_err = fit.sd_beta
        lambda_minus = lambda_first if lambda_first > lambda_second else lambda_second
        lambda_minus_err = lambda_first_err if lambda_first > lambda_second else lambda_second_err
        lambda_plus = lambda_second if lambda_first > lambda_second else lambda_first
        lambda_plus_err = lambda_second_err if lambda_first > lambda_second else lambda_first_err

        fit_data = {
            'lambda-': lambda_minus,
            'lambda-_err': lambda_minus_err,
            'lambda+': lambda_plus,
            'lambda+_err': lambda_plus_err,
            'c1': c1,
            'c1_err': c1_err,
            'c2': c2,
            'c2_err': c2_err,
            'c3': c3,
            'c3_err': c3_err,
            'chi2': chi2,
            'ndf': len(ts[selector]) - len(fit.beta),
            'chi2/ndf': chi2 / (len(ts[selector]) - len(fit.beta))
        }
        h.write_json(self.output()['data'].path, fit_data)


class LocalFitResult(luigi.Task):

    def requires(self):
        return {
            'base': LocalFits(),
            't_up': LocalFits(sys='t_up'),
            't_down': LocalFits(sys='t_down'),
            'bins_up': LocalFits(sys='bins_up'),
            'bins_down': LocalFits(sys='bins_down'),
            'range_up': LocalFits(sys='range_up'),
            'range_down': LocalFits(sys='range_down')
        }

    def output(self):
        return h.data_target('local_fit_results.json', folder='final')

    def run(self):
        h.assure_path('data/final')
        base = h.read_json(self.input()['base']['data'].path)
        t_up = h.read_json(self.input()['t_up']['data'].path)
        t_down = h.read_json(self.input()['t_down']['data'].path)
        bins_up = h.read_json(self.input()['bins_up']['data'].path)
        bins_down = h.read_json(self.input()['bins_down']['data'].path)
        range_up = h.read_json(self.input()['range_up']['data'].path)
        range_down = h.read_json(self.input()['range_down']['data'].path)

        data = {
            'lambda+': base['l+'],
            'lambda+_stat': base['l+_err'],
            'lambda+_sys_t': np.abs(t_up['l+'] - t_down['l+']) / 2.0,
            'lambda+_sys_bins': np.abs(bins_up['l+'] - bins_down['l+']) / 2.0,
            'lambda+_sys_range': np.abs(range_up['l+'] - range_down['l+']) / 2.0,
            'tau+': 1.0 / base['l+'],
            'tau+_stat': base['l+_err'] / base['l+']**2,
            'tau+_sys_t': np.abs(1.0 / t_up['l+'] - 1 / t_down['l+']) / 2.0,
            'tau+_sys_bins': np.abs(1.0 / bins_up['l+'] - 1.0 / bins_down['l+']) / 2.0,
            'tau+_sys_range': np.abs(1.0 / range_up['l+'] - 1.0 / range_down['l+']) / 2.0,
            'lambda-': base['l-'],
            'lambda-_stat': base['l-_err'],
            'lambda-_sys_t': np.abs(t_up['l-'] - t_down['l-']) / 2.0,
            'lambda-_sys_bins': np.abs(bins_up['l-'] - bins_down['l-']) / 2.0,
            'lambda-_sys_range': np.abs(range_up['l-'] - range_down['l-']) / 2.0,
            'tau-': 1.0 / base['l-'],
            'tau-_stat': base['l-_err'] / base['l-']**2,
            'tau-_sys_t': np.abs(1.0 / t_up['l-'] - 1.0 / t_down['l-']) / 2.0,
            'tau-_sys_bins': np.abs(1.0 / bins_up['l-'] - 1.0 / bins_down['l-']) / 2.0,
            'tau-_sys_range': np.abs(1.0 / range_up['l-'] - 1.0 / range_down['l-']) / 2.0,
            'bg': base['c'],
            'bg_stat': base['c_err'],
            'bg_sys_t': np.abs(t_up['c'] - t_down['c']) / 2.0,
            'bg_sys_bins': np.abs(bins_up['c'] - bins_down['c']) / 2.0,
            'bg_sys_range': np.abs(range_up['c'] - range_down['c']) / 2.0,
            'lambda_c': h.lambda_c(base),
            'lambda_c_stat': h.lambda_c_stat(base),
            'lambda_c_sys_t': np.abs(h.lambda_c(t_up) - h.lambda_c(t_down)) / 2.0,
            'lambda_c_sys_bins': np.abs(h.lambda_c(bins_up) - h.lambda_c(bins_down)) / 2.0,
            'lambda_c_sys_range': np.abs(h.lambda_c(range_up) - h.lambda_c(range_down)) / 2.0,
            'R': h.R(base),
            'R_stat': h.R_stat(base),
            'R_sys_t': np.abs(h.R(t_up) - h.R(t_down)) / 2.0,
            'R_sys_bins': np.abs(h.R(bins_up) - h.R(bins_down)) / 2.0,
            'R_sys_range': np.abs(h.R(range_up) - h.R(range_down)) / 2.0,
        }
        h.write_json(self.output().path, data)


class GlobalFitResult(luigi.Task):

    def requires(self):
        return {
            'base': GlobalFit(),
            't_up': GlobalFit(sys='t_up'),
            't_down': GlobalFit(sys='t_down'),
            'bins_up': GlobalFit(sys='bins_up'),
            'bins_down': GlobalFit(sys='bins_down')
        }

    def output(self):
        return h.data_target('global_fit_results.json', folder='final')

    def run(self):
        h.assure_path('data/final')
        base = h.read_json(self.input()['base']['data'].path)
        t_up = h.read_json(self.input()['t_up']['data'].path)
        t_down = h.read_json(self.input()['t_down']['data'].path)
        bins_up = h.read_json(self.input()['bins_up']['data'].path)
        bins_down = h.read_json(self.input()['bins_down']['data'].path)

        data = {
            'lambda+': base['lambda+'],
            'lambda+_stat': base['lambda+_err'],
            'lambda+_sys_t': np.abs(t_up['lambda+'] - t_down['lambda+']) / 2.0,
            'lambda+_sys_bins': np.abs(bins_up['lambda+'] - bins_down['lambda+']) / 2.0,
            'tau+': 1.0 / base['lambda+'],
            'tau+_stat': base['lambda+_err'] / base['lambda+']**2,
            'tau+_sys_t': np.abs(1.0 / t_up['lambda+'] - 1.0 / t_down['lambda+']) / 2.0,
            'tau+_sys_bins': np.abs(1.0 / bins_up['lambda+'] - 1.0 / bins_down['lambda+']) / 2.0,
            'lambda-': base['lambda-'],
            'lambda-_stat': base['lambda-_err'],
            'lambda-_sys_t': np.abs(t_up['lambda-'] - t_down['lambda-']) / 2.0,
            'lambda-_sys_bins': np.abs(bins_up['lambda-'] - bins_down['lambda-']) / 2.0,
            'tau-': 1.0 / base['lambda-'],
            'tau-_stat': base['lambda-_err'] / base['lambda-']**2,
            'tau-_sys_t': np.abs(1.0 / t_up['lambda-'] - 1.0 / t_down['lambda-']) / 2.0,
            'tau-_sys_bins': np.abs(1.0 / bins_up['lambda-'] - 1.0 / bins_down['lambda-']) / 2.0,
            'bg': base['c3'],
            'bg_stat': base['c3_err'],
            'bg_sys_t': np.abs(t_up['c3'] - t_down['c3']) / 2.0,
            'bg_sys_bins': np.abs(bins_up['c3'] - bins_down['c3']) / 2.0,
            'lambda_c': h.lambda_c(base),
            'lambda_c_stat': h.lambda_c_stat(base),
            'lambda_c_sys_t': np.abs(h.lambda_c(t_up) - h.lambda_c(t_down)) / 2.0,
            'lambda_c_sys_bins': np.abs(h.lambda_c(bins_up) - h.lambda_c(bins_down)) / 2.0,
            'R': h.R(base),
            'R_stat': h.R_stat(base),
            'R_sys_t': np.abs(h.R(t_up) - h.R(t_down)) / 2.0,
            'R_sys_bins': np.abs(h.R(bins_up) - h.R(bins_down)) / 2.0,
        }
        h.write_json(self.output().path, data)


class FullAnalysis(luigi.Task):

    def requires(self):
        return [DecayPlots(), Efficiency(), LocalFitResult(), GlobalFitResult()]

    def run(self):
        pass
