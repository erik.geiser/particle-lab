import os
import json
from pprint import pprint
from glob import glob
from collections import OrderedDict

import numpy as np
import luigi


basepath = os.path.dirname(os.path.realpath(__file__))


def assure_path(*paths):
    for path in paths:
        full_path = os.path.join(basepath, path)
        if not os.path.isdir(full_path):
            os.makedirs(full_path)


def full_path(*fname):
    return os.path.join(basepath, *fname)


def plot_path(fname, folder=''):
    return os.path.join(basepath, 'plots', folder, fname)


def plot_target(fname, folder=''):
    fname = os.path.basename(fname)
    return luigi.LocalTarget(plot_path(fname, folder=folder))


def data_target(fname, folder=''):
    fname = os.path.basename(fname)
    return luigi.LocalTarget(os.path.join(basepath, 'data', folder, fname))


def read_json(name):
    with open(name) as data_file:
        data = json.load(data_file)
    return data


def write_json(name, data):
    ordered_data = OrderedDict(sorted(data.items()))
    with open(name, 'w') as data_file:
        print('\n[i] %s:' % name)
        pprint(data)
        print('\n\n')
        json.dump(ordered_data, data_file, indent=4)


def decay_global((lambda_minus, lambda_plus, c1, c2, c3), t):
    return c1 * np.exp(-lambda_minus * t) + c2 * np.exp(-lambda_plus * t) + c3


def decay_local((lambda_x, c), t):
    return c - lambda_x * t


def error_local((lambda_err, c_err), t):
    return np.sqrt((t * lambda_err)**2 + c_err**2)


def error_local_m((lambda_err, c_err), t):
    return t * lambda_err


def decay_const((c,), t):
    return c * np.ones_like(t)


def error_const((c_err,), t):
    return c_err * np.ones_like(t)


def symlog(x):
    res = np.zeros_like(x)
    res[x > 0] = np.log(x[x > 0])
    res[x < 0] = -np.log(-x[x < 0])
    res[x == 0] = 0
    return res


def lambda_c(data):
    lp = 'lambda+' if 'lambda+' in data.keys() else 'l+'
    lm = 'lambda-' if 'lambda-' in data.keys() else 'l-'
    return data[lm] - data[lp]


def lambda_c_stat(data):
    lp_err = 'lambda+_err' if 'lambda+_err' in data.keys() else 'l+_err'
    lm_err = 'lambda-_err' if 'lambda-_err' in data.keys() else 'l-_err'
    return np.sqrt(data[lm_err]**2 + data[lp_err]**2)


def R(data):
    c1 = data['c1' if 'c1' in data.keys() else 'l-_const']
    c2 = data['c2' if 'c2' in data.keys() else 'l+_const']
    return c2 / c1


def R_stat(data):
    c1 = data['c1' if 'c1' in data.keys() else 'l-_const']
    c2 = data['c2' if 'c2' in data.keys() else 'l+_const']
    c1_err = data['c1_err' if 'c1_err' in data.keys() else 'l-_const_err']
    c2_err = data['c2_err' if 'c2_err' in data.keys() else 'l+_const_err']
    return np.sqrt((c2_err / c1)**2 + (c2 * c1_err / c1**2)**2)


calib_files = {
    int(os.path.basename(delay).replace('.TKA', '')): data_target(delay, folder='time_calib')
    for delay in glob('data/time_calib/*.TKA')
}
