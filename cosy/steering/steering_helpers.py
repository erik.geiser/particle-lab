import os
from collections import namedtuple


basepath = os.path.dirname(os.path.realpath(__file__))


steerers_exact = {
    # steerer_number: (lower_current, upper_current)
    7: (-14.970, 15.018),
    16: (-29.963, 30.012),
    21: (-30.940, 28.987),
    33: (-14.070, 20.000),
    41: (-2.955, 5.983),
}

steerers = {
    # steerer_number: (lower_current, upper_current)
    7: (-15, 15),
    16: (-30, 30),
    21: (-30, 30),
    33: (-15, 20),
    41: (-4, 5),
}


def current_str(current):
    fmt = 'p%02d' if current > 0 else 'm%02d'
    return fmt % abs(current)


def get_filenames(steerer, subdir='', postfix='dat'):
    fmt = os.path.join(basepath, 'data', subdir, 'je_s%02d_%s.%s')
    return [
        fmt % (steerer, current_str(current), postfix)
        for current in steerers[steerer]
    ]


def info(fname):
    fname = os.path.basename(fname).replace('p', '+').replace('m', '-')
    steerer = int(fname[4:6])
    currents = steerers_exact[steerer]
    current = currents[0] if int(fname[7:10]) < 0 else currents[1]
    Info = namedtuple('Info', ['steerer', 'current'])
    return Info(steerer=steerer, current=current)


def assure_path(path):
    path = os.path.join(basepath, path)
    if not os.path.isdir(path):
        os.makedirs(path)


numpy_files = [
    os.path.join(basepath, 'data/processed/je_ref.npy'),
    os.path.join(basepath, 'data/processed/result_2.npy'),
]
csv_files = [
    os.path.join(basepath, 'data/je_ref.dat'),
    os.path.join(basepath, 'data/result_2.dat')
]

for steerer in steerers.keys():
    csv_files += get_filenames(steerer)
    numpy_files += get_filenames(steerer, subdir='processed', postfix='npy')
