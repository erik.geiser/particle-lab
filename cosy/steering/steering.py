import os

import numpy as np
import matplotlib.pyplot as plt
import luigi
from tqdm import tqdm

import steering_helpers as h


class ProcessData(luigi.Task):

    def output(self):
        return [luigi.LocalTarget(fname) for fname in h.numpy_files]

    def run(self):
        h.assure_path('data/processed')

        for csv_fname, np_fname in zip(h.csv_files, h.numpy_files):
            name, coord, pos, dis, ref = np.genfromtxt(csv_fname, delimiter=':', unpack=True,
                                                       usecols=(0, 1, 2, 3, 4), comments='#',
                                                       dtype='str', skip_header=2)

            selector = np.array([i for i in range(len(name)) if name[i].startswith('bpmx')])
            pos = pos.astype(np.float64)[selector]
            dis = dis.astype(np.float64)[selector]
            ref = ref.astype(np.float64)[selector]

            np.save(np_fname, np.array([pos, dis, ref]))


class PlotData(luigi.Task):

    FILETYPE = '.pdf'

    def input_to_output(self, input_path):
        return os.path.join('plots', os.path.basename(input_path).replace('.npy', self.FILETYPE))

    def requires(self):
        return ProcessData()

    def output(self):
        return [
            luigi.LocalTarget(self.input_to_output(dataset.path))
            for dataset in self.requires().output()
        ]

    def run(self):
        h.assure_path('plots')

        for dataset in tqdm(self.requires().output()):
            data = np.load(dataset.path)
            pos = data[0]
            dis = data[1]
            ref = data[2]

            fig, ax = plt.subplots()
            ax.axhline(0, color='dimgrey', linewidth=0.8)
            ax.plot(pos, ref, 'o-', label='Reference', color='darkgrey',
                    linewidth=2.5)
            label = 'Corrected Beam Position' if 'result' in dataset.path else 'Measurement'
            ax.plot(pos, dis, 'ro-', label=label)
            if 'ref' in dataset.path:
                title = 'Reference Measurement'
            elif 'result' in dataset.path:
                title = None
            else:
                info = h.info(dataset.path)
                title = 'Steerer %02d: %.2f%%' % (info.steerer, info.current)
            ax.legend(loc='best', title=title)
            ax.set_xlabel('Beam Pipe Position [m]')
            ax.set_ylabel('Beam Displacement [mm]')
            ax.set_ylim(-40, 40)
            fig.savefig(self.input_to_output(dataset.path))


class ConstructMatrix(luigi.Task):

    def requires(self):
        return ProcessData()

    def output(self):
        return {
            'img': luigi.LocalTarget(os.path.join(h.basepath, 'plots/A.pdf')),
            'data': luigi.LocalTarget(os.path.join(h.basepath, 'data/processed/A.npy'))
        }

    def run(self):
        A = None

        for i, steerer in enumerate(sorted(h.steerers_exact.keys())):
            lower, upper = h.get_filenames(steerer, subdir='processed', postfix='npy')
            lower, upper = np.load(lower), np.load(upper)
            lower_current, upper_current = h.steerers_exact[steerer]
            delta_d = upper[1] - lower[1]
            print(delta_d)
            delta_I = upper_current - lower_current

            if A is None:
                A = np.zeros((delta_d.size, 5))

            for j in range(delta_d.size):
                A[j, i] = delta_d[j] / delta_I

        np.save(self.output()['data'].path, A)

        width, height = A.shape

        fig, ax = plt.subplots()
        ax.imshow(np.abs(A), interpolation='nearest', cmap='plasma')

        for x in range(width):
            for y in range(height):
                ax.annotate('%.2f' % A[x, y], xy=(y, x), size=5,
                            horizontalalignment='center', color='darkgrey',
                            verticalalignment='center')

        ax.set_yticks(range(width))
        ax.set_yticklabels(['$d_{%d}$' % i for i in range(width)])
        ax.set_ylabel('Displacements')

        ax.set_xticks(range(height))
        ax.set_xticklabels(['S%02d' % i for i in sorted(h.steerers.keys())])
        ax.set_xlabel('Steerer')

        ax.tick_params(axis='both', which='major', labelsize=5)
        fig.savefig(self.output()['img'].path)


class TestMatrix(luigi.Task):

    def requires(self):
        return {
            'A': ConstructMatrix(),
            'data': ProcessData(),
        }

    def output(self):
        return []

    def run(self):
        A = np.load(self.requires()['A'].output()['data'].path)
        np.set_printoptions(linewidth=500, precision=2)
        print(A)

        for i, steerer in enumerate(sorted(h.steerers_exact.keys())):
            lower, upper = h.get_filenames(steerer, subdir='processed', postfix='npy')
            lower, upper = np.load(lower), np.load(upper)
            lower_current, upper_current = np.zeros(5), np.zeros(5)
            lower_current[i] = h.steerers_exact[steerer][0]
            upper_current[i] = h.steerers_exact[steerer][1]

            print('\n\n###########################################################')
            print('Upper Current: %s' % upper_current)
            print('d = %s' % (upper[1] - upper[2]))
            print('A * I = %s ' % np.dot(A, upper_current.reshape(5, 1)).flatten())
            print('-----------------------------------------------------------')
            print('Lower Current: %s' % lower_current)
            print('d = %s' % (lower[1] - lower[2]))
            print('A * I = %s ' % np.dot(A, lower_current.reshape(5, 1)).flatten())
            print('###########################################################\n\n')


class InvertMatrix(luigi.Task):

    def requires(self):
        return {
            'A': ConstructMatrix(),
            'ref': ProcessData(),
        }

    def output(self):
        return {
            'I': luigi.LocalTarget(os.path.join(h.basepath, 'plots/I.pdf')),
            'expected': luigi.LocalTarget(os.path.join(h.basepath, 'plots/expected.pdf')),
        }

    def run(self):
        A = np.load(self.requires()['A'].output()['data'].path)
        data = np.load(os.path.join(h.basepath, 'data/processed/je_ref.npy'))
        I = -np.dot(np.linalg.pinv(A), data[2].reshape(-1, 1))

        width, height = I.shape

        fig, ax = plt.subplots()
        ax.imshow(np.abs(np.array(I)), interpolation='nearest', cmap='summer')

        for x in range(width):
            for y in range(height):
                ax.annotate('%02d%%' % int(I[x, y]), xy=(y, x),
                            horizontalalignment='center',
                            verticalalignment='center')

        ax.set_yticks(range(5))
        ax.set_yticklabels(('$I_{S%02d}$' % i for i in sorted(h.steerers.keys())))
        ax.set_xticks([])
        fig.savefig(self.output()['I'].path)

        fig, ax = plt.subplots()
        ax.axhline(0, color='dimgrey', linewidth=0.8)
        ax.plot(data[0], np.dot(A, I), 'ro-', label='Expected Orbit')
        ax.plot(data[0], data[2], '--', color='darkgrey', label='Reference Orbit')
        ax.legend(loc='best')
        ax.set_xlabel('Beam Pipe Position [m]')
        ax.set_ylabel('Beam Displacement [mm]')
        ax.set_ylim(-40, 40)
        fig.savefig(self.output()['expected'].path)


class FullAnalysis(luigi.Task):

    def requires(self):
        return [PlotData(), InvertMatrix()]
