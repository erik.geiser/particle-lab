import os
from collections import namedtuple

import numpy as np
import luigi
from tqdm import tqdm
from glob import glob


basepath = os.path.dirname(os.path.realpath(__file__))

Measurement = namedtuple('Measurement', ('x', 'y', 'B'))


# 0.5V, 40.9A
class ProcessData(luigi.Task):

    ROUND = True

    def output(self):
        return {
            'plane': luigi.LocalTarget(os.path.join(basepath, 'data/processed/plane.npy')),
            'circle': luigi.LocalTarget(os.path.join(basepath, 'data/processed/circle.npy')),
            'better_circle': luigi.LocalTarget(os.path.join(basepath,
                                                            'data/processed/better_circle.npy')),
            'edge': luigi.LocalTarget(os.path.join(basepath, 'data/processed/edge.npy')),
        }

    def run(self):
        assure_path('data/processed')
        assure_path('plots')

        circle_points, better_circle_points, plane_points, edge_points = [], [], [], []

        B_n, Bx_n, By_n, Bz_n = load_file(os.path.join(basepath, 'data/noise.txt')) * 1000
        B_n, Bx_n, By_n, Bz_n = np.mean(B_n), np.mean(Bx_n), np.mean(By_n), np.mean(Bz_n)
        B_nb, Bx_nb, By_nb, Bz_nb = load_file(os.path.join(basepath,
                                              'data/noise_better.txt')) * 1000
        B_nb, Bx_nb, By_nb, Bz_nb = np.mean(B_nb), np.mean(Bx_nb), np.mean(By_nb), np.mean(Bz_nb)

        for fname in tqdm(glob('data/*.txt')):
            if 'noise' in fname:
                continue

            try:
                B, Bx, By, Bz = load_file(fname) * 1000
            except:
                print('[!] Error in %s' % fname)
                continue

            n = B.size
            x, y = coords(fname)
            B_err, Bx_err, By_err, Bz_err = (error(x) / np.sqrt(n) for x in (B, Bx, By, Bz))
            B, Bx, By, Bz = np.mean(B), np.mean(Bx), np.mean(By), np.mean(Bz)

            # noise correction
            if filetype(fname) == 'better_circle':
                B, Bx, By, Bz = B - B_nb, Bx - Bx_nb, By - By_nb, Bz - Bz_nb
            else:
                B, Bx, By, Bz = B - B_n, Bx - Bx_n, By - By_n, Bz - Bz_n

            if filetype(fname) == 'plane':
                x, y = coords(fname)
                plane_points.append([x, y, B, B_err, Bx, Bx_err, By, By_err, Bz, Bz_err])
            elif filetype(fname) == 'edge':
                r, phi = coords(fname, cartesian=False)
                edge_points.append([r, phi, B, B_err, Bx, Bx_err, By, By_err, Bz, Bz_err])
            elif filetype(fname) == 'circle':
                r, phi = coords(fname, cartesian=False)
                circle_points.append([r, phi, B, B_err, Bx, Bx_err, By, By_err, Bz, Bz_err])
            elif filetype(fname) == 'better_circle':
                r, phi = coords(fname, cartesian=False, radius=30)
                better_circle_points.append([r, phi, B, B_err, Bx, Bx_err, By, By_err, Bz, Bz_err])
            else:
                raise RuntimeError('Unhandled filetype: %s' % filetype(fname))

        print('###')
        print(len(circle_points), len(circle_points[0]))
        print(len(better_circle_points), len(better_circle_points[0]))
        print("###")
        np.save(self.output()['plane'].path, np.array(plane_points))
        np.save(self.output()['circle'].path, np.array(circle_points))
        np.save(self.output()['better_circle'].path, np.array(better_circle_points))
        np.save(self.output()['edge'].path, np.array(edge_points))


def filename(x, z):
    x_sign = 'm' if x < 0 else ''
    z_sign = 'm' if z < 0 else ''
    return '%s%d_%s%d.txt' % (x_sign, x, z_sign, z)


def add_file(output, name, prefix='plots', postfix='.pdf'):
    output[name] = luigi.LocalTarget(os.path.join(basepath, prefix, name + postfix))


def load_file(fname):
    return np.genfromtxt(fname, usecols=(1, 2, 3, 4), skip_header=1,
                         unpack=True)


def error(x):
    return np.std(x, ddof=1)


def irnd(x):
    return int(round(x))


def filetype(filename):
    if '_' in filename:
        return 'plane'
    if '-' in filename:
        if 'e' in filename:
            return 'edge'
        else:
            return 'circle'
    else:
        return 'better_circle'


def load_circle_prepared(filename):
    data = np.load(filename)
    rs, phis, Bs, Bs_err, Bxs, Bxs_err, Bys, Bys_err, Bzs, Bzs_err = data.T
    idxs = np.argsort(phis)
    phis = phis[idxs]
    rs = rs[idxs]
    Bs = Bs[idxs]
    Bs_err = Bs_err[idxs]
    Bxs = Bxs[idxs]
    Bxs_err = Bxs_err[idxs]
    Bys = Bys[idxs]
    Bys_err = Bys_err[idxs]
    Bzs = Bzs[idxs]
    Bzs_err = Bzs_err[idxs]

    xs = rs * np.cos(phis)
    xs_err = np.abs(rs * -np.sin(phis) * 0.26 / np.sqrt(12))
    ys = -rs * np.sin(phis)
    ys_err = np.abs(rs * np.cos(phis) * 0.26 / np.sqrt(12))

    Brs = (Bxs * xs + Bys * ys) / rs
    Brs_err = rs**-1 * np.sqrt(np.clip(xs**2 * Bxs_err**2 + ys**2 * Bys_err**2 +
                                       (Bxs * xs_err / rs)**2 + (Bys * ys_err / rs)**2,
                                       a_min=0, a_max=None))
    Bphis = (Bys * xs - Bxs * ys) / rs
    Bphis_err = rs**-1 * np.sqrt(np.clip(xs**2 * Bys_err**2 - ys**2 * Bxs_err**2 +
                                         (Bys * xs_err / rs)**2 + (-Bxs * ys_err / rs)**2,
                                         a_min=0, a_max=None))
    Bcomb = Bphis + 1j * Brs
    Bcomb_err = Bphis_err + 1j * Brs_err

    Bxzs = np.sqrt(Bxs**2 + Bzs**2)
    Bxzs_err = np.sqrt((Bxs / Bxzs)**2 * Bxs_err**2 + (Bys / Bxzs)**2 * Bzs_err**2)

    Dataset = namedtuple('Dataset', ('rs', 'phis', 'xs', 'ys', 'Bs', 'Bs_err', 'Bxs', 'Bxs_err',
                                     'Bys', 'Bys_err', 'Bzs', 'Bzs_err', 'Bxzs', 'Bxzs_err', 'Brs',
                                     'Brs_err', 'Bphis', 'Bphis_err', 'Bcomb', 'Bcomb_err'))
    return Dataset(rs, phis, xs, ys, Bs, Bs_err, Bxs, Bxs_err, Bys, Bys_err,
                   Bzs, Bzs_err, Bxzs, Bxzs_err, Brs, Brs_err, Bphis, Bphis_err, Bcomb, Bcomb_err)


def coords(filename, radius=12, cartesian=True):
    full_fname = filename
    filename = os.path.basename(filename).replace('.txt', '')
    if '_' in filename:
        if not cartesian:
            raise RuntimeError('Non cartesian coordinates are only'
                               'supported for circle measurement')
        c = filename.split('_')
        for i in range(len(c)):
            c[i] = int(c[i].replace('m', '-'))
        return tuple(c)
    elif '-' in filename:
        phi = float(filename.replace('-', '.').replace('e', '')) * np.pi / 6.0
        if cartesian:
            return radius * np.cos(phi), radius * np.sin(phi)
        else:
            return radius, phi
    elif filename.isdigit():
        return radius, float(filename) * 0.001
    else:
        raise RuntimeError('Filename %s containes neither _ (plane) nor - (circle)'
                           ' and is not digit only' % full_fname)


def assure_path(path):
    path = os.path.join(basepath, path)
    if not os.path.isdir(path):
        os.makedirs(path)


def grid2fitpoints(xmin, ymin, xmax, ymax):
    xlin = np.linspace(xmin, xmax)
    ylin = np.linspace(ymin, ymax)
    xx, yy = np.meshgrid(xlin, ylin)
    return np.concatenate(np.array([xx, yy]).T, axis=0)


def fourier_coeffs(quantity, error, name):
    spectrum = np.fft.fft(quantity)
    real = spectrum.real
    imag = spectrum.imag
    diffs = np.fft.fft(quantity + error) - np.fft.fft(quantity - error)
    real_err, imag_err = np.abs(diffs.real), np.abs(diffs.imag)
    print real_err, imag_err
    if name == '$B_r$':
        return {'A': real[:10], 'A_err': real_err[:10], 'B': imag[1:10], 'B_err': imag_err[1:10],
                'nA': np.arange(10), 'nB': np.arange(1, 10), 'type': 'comb',
                'Afull': real[:10], 'Bfull': imag[:10]}
    elif name == '$B_{\phi}$':
        return {'A': -imag[1:10], 'A_err': imag_err[1:10], 'B': real[:10], 'B_err': real_err[:10],
                'nA': np.arange(1, 10), 'nB': np.arange(10), 'type': 'phi',
                'Afull': -imag[:10], 'Bfull': real[:10]}
    elif name == '$B_{\phi} + i\cdot B_r$':
        return {'A': imag[1:10], 'A_err': imag_err[1:10], 'B': real[:10], 'B_err': real_err[:10],
                'nA': np.arange(1, 10), 'nB': np.arange(10),
                'Afull': imag[:10], 'Bfull': real[:10]}
    else:
        raise ValueError('Invalid quantity: %s' % quantity)
