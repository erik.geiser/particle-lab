import os

import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import lombscargle
from scipy.optimize import curve_fit
from scipy.odr import ODR, RealData, Model
import luigi
from tabulate import tabulate
from tqdm import tqdm

import quadrupole_helpers as h


class FFTs(luigi.Task):

    def requires(self):
        return h.ProcessData()

    def output(self):
        return {
            'fft': luigi.LocalTarget(os.path.join(h.basepath, 'plots/fft.pdf')),
            'fft_edge': luigi.LocalTarget(os.path.join(h.basepath, 'plots/fft_edge.pdf')),
            'Br': luigi.LocalTarget(os.path.join(h.basepath, 'data/processed/Br.tex')),
            'Bphi': luigi.LocalTarget(os.path.join(h.basepath, 'data/processed/Bphi.tex')),
            'Br_edge': luigi.LocalTarget(os.path.join(h.basepath, 'data/processed/Br_edge.tex')),
            'Bphi_edge': luigi.LocalTarget(os.path.join(h.basepath, 'data/processed/Bphi_edge.tex'))
        }

    def run(self):
        h.assure_path('plots')
        h.assure_path('data/processed')
        m = h.load_circle_prepared(self.requires().output()['circle'].path)
        e = h.load_circle_prepared(self.requires().output()['edge'].path)
        fig, axar = plt.subplots(2, figsize=(6, 8))
        fig_e, axar_e = plt.subplots(2, figsize=(6, 8))

        for ax, quantity, err, name, key in zip((axar[0], axar[1], axar_e[0], axar_e[1]),
                                                (m.Brs, m.Bphis, e.Brs, e.Bphis),
                                                (m.Brs_err, m.Bphis_err, e.Brs_err, e.Bphis_err),
                                                ('$B_r$', '$B_{\phi}$') * 2,
                                                ('Br', 'Bphi', 'Br_edge', 'Bphi_edge')):
            fft = h.fourier_coeffs(quantity, err, name)
            ax.set_title(name)
            ax.errorbar(fft['nB'], fft['B'], yerr=fft['B_err'], fmt='x',
                        color='orange', label='$\mathcal{B}_n$', capsize=1.4)
            ax.errorbar(fft['nA'], fft['A'], yerr=fft['A_err'],
                        fmt='x', color='blue', label='$\mathcal{A}_n$', capsize=1.4)
            ax.axhline(0, color='dimgrey')
            ax.axvline(0, alpha=0.2, color='dimgrey')
            ax.axvline(2, alpha=0.2, color='g', label='Quadrupole')
            ax.set_xticks(np.arange(10))
            ax.legend()

            tabdata = [
                ['$\mathcal{A}_n$'] + list(fft['Afull'][:5]),
                ['$\mathcal{B}_n$'] + list(fft['Bfull'][:5]),
            ]
            headers = ['', '0', '1', '2', '3', '4', '5']
            with open(self.output()[key].path, 'w') as out:
                out.write(tabulate(tabdata, headers=headers, tablefmt='latex', stralign='center'))

        fig.tight_layout()
        fig.savefig(self.output()['fft'].path)
        fig_e.tight_layout()
        fig_e.savefig(self.output()['fft_edge'].path)


class PlaneFit(luigi.Task):

    def requires(self):
        return h.ProcessData()

    def output(self):
        return {
            'imgBr': luigi.LocalTarget(os.path.join(h.basepath, 'plots/PlaneFitBr.png')),
            'imgBphi': luigi.LocalTarget(os.path.join(h.basepath, 'plots/PlaneFitPhi.png'))
        }

    def run(self):
        h.assure_path('plots')
        data = np.load(self.requires().output()['plane'].path)
        xs, ys, Bs, Bs_err, Bxs, Bxs_err, Bys, Bys_err, Bzs, Bzs_err = data.T
        measurements = [h.Measurement(*row[:3]) for row in data]

        cnc = 2
        minimum = min(measurements, key=lambda meas: meas.B)

        xs = ((xs - minimum.x) * cnc)
        ys = -((ys - minimum.y) * cnc)

        rs = np.sqrt(xs**2 + ys**2)
        # phis = np.arctan2(ys, xs)

        Brs = (Bxs * xs + Bys * ys) / (np.sqrt(xs**2 + ys**2))
        Brs = Brs[(xs != 0) & (ys != 0)]

        Bphis = (Bys * xs - Bxs * ys) / (np.sqrt(xs**2 + ys**2))
        Bphis = Bphis[(ys != 0) & (xs != 0)]

        Brs_err = rs**-1 * np.sqrt(xs**2 * Bxs_err**2 +
                                   ys**2 * Bys_err**2)
        Brs_err = Brs_err[(xs != 0) & (ys != 0)]
        Bphis_err = rs**-1 * np.sqrt(xs**2 * Bys_err**2 -
                                     ys**2 * Bxs_err**2)
        Bphis_err = Bphis_err[(xs != 0) & (ys != 0)]

        fitpoints_x = xs[(xs != 0) & (ys != 0)]
        fitpoints_y = ys[(ys != 0) & (xs != 0)]

        fitpoints = np.empty((2, fitpoints_x.size))
        fitpoints[0] = fitpoints_x
        fitpoints[1] = fitpoints_y
        fitpoints = fitpoints.T

        def Eq49(x, y, a1, a2, a3, a4, b1, b2, b3, b4):
            As = [a1, a2, a3, a4]
            Bs = [b1, b2, b3, b4]
            r = np.sqrt(x**2 + y**2)
            phi = np.arctan2(y, x)

            r0 = 30

            return np.sum([(r / r0)**(n - 1) * (Bs[n - 1] + 1j * As[n - 1]) *
                          (np.cos(2 * n * phi) + 1j * np.sin(2 * n * phi))
                          for n in range(1, 5)], axis=0)

        def Eq49_fit(data, a1, a2, a3, a4, b1, b2, b3, b4):
            x = data[:, 0]
            y = data[:, 1]
            return Eq49(x, y, a1, a2, a3, a4, b1, b2, b3, b4).imag

        Brs = np.nan_to_num(Brs)
        Bphis = np.nan_to_num(Bphis)
        Brs_err = np.nan_to_num(Brs_err)
        Bphis_err = np.nan_to_num(Bphis_err)

        print(fitpoints.shape, "Shapes", Brs.shape)

        popt, pcov = curve_fit(Eq49_fit, fitpoints, Brs, sigma=Brs_err)

        print("Fit results : ##############*************")
        print("A:", popt[0:4],)
        print("B:", popt[4:8],)
        print(np.sqrt(np.diag(pcov)))
        print("############################*************")

        xlin = np.linspace(xs.min(), xs.max())
        ylin = np.linspace(ys.min(), ys.max())
        xg, yg = np.meshgrid(xlin, ylin)

        print("fitpoints", fitpoints.shape)
        print("grid", h.grid2fitpoints(xs.min(), ys.min(), xs.max(), ys.max()).shape)

        fig, ax = plt.subplots()
        res = Eq49(xg, yg, *popt)
        # ax.contourf(xlin, ylin, res)
        ax.imshow(res.imag, origin='lower', extent=(xs.min(), xs.max(), ys.min(), ys.max()))
        fig.savefig(self.output()['imgBr'].path)

        fig, ax = plt.subplots()
        res = Eq49(xg, yg, *popt)
        # ax.contourf(xlin, ylin, res)
        ax.imshow(res.real, origin='lower', extent=(xs.min(), xs.max(), ys.min(), ys.max()))
        fig.savefig(self.output()['imgBphi'].path)
