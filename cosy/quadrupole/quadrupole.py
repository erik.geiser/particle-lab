import os
from itertools import product

import numpy as np
import matplotlib.pyplot as plt
import luigi
from tqdm import tqdm

import quadrupole_helpers as h
from quadrupole_analysis import FFTs, PlaneFit


class PlotPlane(luigi.Task):

    def requires(self):
        return h.ProcessData()

    def output(self):
        return {
            'img': luigi.LocalTarget(os.path.join(h.basepath, 'plots/plane.pdf')),
            'map': luigi.LocalTarget(os.path.join(h.basepath, 'data/processed/map.npy')),
            'min': luigi.LocalTarget(os.path.join(h.basepath, 'data/processed/min.txt')),
        }

    def run(self):
        data = np.load(self.requires().output()['plane'].path)
        xs, ys, Bs, Bs_err, Bxs, Bxs_err, Bys, Bys_err, Bzs, Bzs_err = data.T
        measurements = [h.Measurement(*row[:3]) for row in data]

        min_x, max_x = np.min(xs), np.max(xs)
        min_y, max_y = np.min(ys), np.max(ys)
        width = h.irnd(max_x - min_x + 1)
        height = h.irnd(max_y - min_y + 1)

        B_map = np.zeros((width, height))
        B_map[B_map == 0] = np.nan

        for x, y, B in zip(xs, ys, Bs):
            B_map[int(x - min_x)][int(y - min_y)] = B

        minimum = min(measurements, key=lambda meas: meas.B)
        print("Minimum is (%d, %d)" % (minimum.x, minimum.y))
        print("Shifted minimum is (%d, %d)" % (minimum.x - min_x, minimum.y - min_y))

        np.save(self.output()['map'].path, B_map)
        np.savetxt(self.output()['min'].path, np.array([[minimum.x, minimum.y, minimum.B,
                   minimum.x - min_x, minimum.y - min_y]]), header='x, y, B, x\', y\'')

        fig, ax = plt.subplots()
        ranges = (2 * (minimum.x - min_x), 2 * (minimum.x - max_x),
                  -2 * (minimum.y - min_y), -2 * (minimum.y - max_y))
        img = ax.imshow(1000 * B_map.T, origin='lower', extent=ranges)
        ax.annotate('x', xy=(0, 0), size=15, color='red', fontsize=2.5,
                    horizontalalignment='center', verticalalignment='center')
        ax.set_xlabel('x [mm]')
        ax.set_ylabel('z [mm]')
        cb = fig.colorbar(img)
        cb.set_label('$|\\vec{B}|$ [mT]', rotation='horizontal')
        fig.savefig(self.output()['img'].path)


class PlotCircle(luigi.Task):

    def requires(self):
        return h.ProcessData()

    def output(self):
        outputs = {}
        for name, postfix in product(['polar', 'polar_Br', 'polar_Bphi', 'B', 'Br', 'Bphi', 'Bxz'],
                                     ['', '_edge']):
            h.add_file(outputs, 'circle_' + name + postfix)
        return outputs

    def run(self):
        h.assure_path('plots')
        normal = h.load_circle_prepared(self.requires().output()['circle'].path)
        edge = h.load_circle_prepared(self.requires().output()['edge'].path)

        for data, postfix in zip([normal, edge], ['', '_edge']):

            for quantity, name, shortname in zip((data.Brs, data.Bphis),
                                                 ('$|B_r|$', '$|B_{\phi}|$'), ('Br', 'Bphi')):
                fig, ax = plt.subplots(subplot_kw={'projection': 'polar'})
                ax.plot(data.phis, np.abs(quantity), 'ro')
                ax.set_rmax(np.max(np.abs(quantity)) * 1.4)
                if data == normal:
                    ax.set_rticks([0, 0.05, 0.1])
                else:
                    ax.set_rticks([0, 0.05])
                ax.set_ylabel('%s [mT]' % name, labelpad=-240, rotation='horizontal')
                ax.set_rlabel_position(0)
                ax.grid(True)
                fig.savefig(self.output()['circle_polar_' + shortname + postfix].path)

            fig, ax = plt.subplots(subplot_kw={'projection': 'polar'})
            ax.plot(data.phis, data.rs, 'ro')
            ax.set_rmax(np.max(data.rs) * 1.15)
            ax.set_rticks([data.rs[0]])
            ax.set_ylabel('r [CNC units]', labelpad=-200, rotation='horizontal')
            ax.set_rlabel_position(0)
            ax.grid(True)
            fig.savefig(self.output()['circle_polar' + postfix].path)

        packed_data = [(normal.Bs, normal.Bs_err, edge.Bs, edge.Bs_err),
                       (normal.Brs, normal.Brs_err, edge.Brs, edge.Brs_err),
                       (normal.Bphis, normal.Bphis_err, edge.Bphis, edge.Bphis_err)]
        names = ['B', 'Br', 'Bphi']
        nicenames = ['$|\\vec{B}|$', '$B_r$', '$B_{\phi}$']

        for (val, err, val2, err2), name, nicename in zip(packed_data, names, nicenames):
            fig, ax = plt.subplots()
            ax.errorbar(normal.phis, val, yerr=err, label='Inner')
            ax.errorbar(edge.phis, val2, yerr=err2, label='Edge')
            ax.set_xlabel('$\phi$ [rad]')
            ax.set_ylabel('%s [mT]' % nicename)
            ax.axhline(0, color='dimgrey')
            ax.set_title(nicename)
            ax.legend()
            fig.savefig(self.output()['circle_' + name].path)


class PlotNoise(luigi.Task):

    def output(self):
        return luigi.LocalTarget('plots/noise.pdf')

    def run(self):
        h.assure_path('plots')
        B, Bx, By, Bz = h.load_file(os.path.join(h.basepath, 'data/noise.txt')) * 1000
        fig, axar = plt.subplots(2, 2)
        for ax, quantity, name in zip(axar.flatten(), (B, Bx, By, Bz),
                                      ('$|\\vec{B}|$', '$B_x$', '$B_y$', '$B_z$')):
            ax.hist(quantity, bins=20)
            mean, std = np.mean(quantity), np.std(quantity, ddof=1)
            ax.axvspan(mean - std, mean + std, color='red', alpha=0.2, linewidth=1,
                       label='$\sigma=%.3f$' % std)
            ax.axvline(mean, color='red', alpha=0.8, linewidth=2.5,
                       label='$\mu=%.3f$' % mean)
            ax.set_xlabel('%s [mT]' % name)
            ax.set_ylabel('#')
            ax.legend(loc='best', prop={'size': 6})
        fig.tight_layout()
        fig.savefig(self.output().path)


class FullAnalysis(luigi.Task):

    def output(self):
        return []

    def requires(self):
        return [PlotPlane(), PlotCircle(), PlotNoise(), FFTs(), PlaneFit()]
